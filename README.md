# Parkme - CAD - PDV Web App

This project uses Onsenui v2  
Check the documentation page [https://onsen.io/v2/](https://onsen.io/v2/)

## Requirements

* [Sass](http://sass-lang.com/install)
* [NodeJS](https://nodejs.org/en/download/)

## Installation

To install and run execute the command `npm install && gulp build`  
This command will create a *dist* directory

## Start

To start the application run `gulp` on projects folder

## Coding

The source code stays on *src* folder  
To create deployment code check the [deployment](#markdown-header-deployment) section

## Styles

The styles stays on *scss* folder  
It use `sass` to

## Deployment

### Staging

execute the command

```
git remote add staging root@104.236.103.213:/srv/repo/parkme-webapp.git
```

> Note: You must have the permissions (key pair) to push to repository

To deploy the application push the code to staging on master branch

```
git push staging master
```
