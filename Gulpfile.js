var gulp = require('gulp');
var browserify = require('browserify');
var babelify = require('babelify');
var buffer = require('vinyl-buffer')
var source = require('vinyl-source-stream');
var htmlmin = require('gulp-htmlmin');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var merge = require('merge-stream');
var uglifycss = require('gulp-uglifycss');
var connect = require('gulp-connect');
var opn = require('opn');
var clean = require('gulp-clean');
var replace = require('gulp-replace');
var fs = require('fs');
var closureCompiler = require('gulp-closure-compiler');
var serverPublicFolder = '../services/public';
var serverIndexFolder = '../services/resources/views';
var s3Host = 'https://s3-sa-east-1.amazonaws.com/zona-azul/';

var server = {
    host: 'localhost',
    port: '6739'
}

var uploadFiles = [
    "../services/public/logo.png",
    "./dist/app.min.css",
    "./dist/app.min.js"
];

gulp.task("upload", function() {
    var config = JSON.parse(fs.readFileSync('../aws.json'));
    var s3 = require('gulp-s3-upload')(config);
    gulp.src("../services/public/images/*")
        .pipe(s3({
            Bucket: 'zona-azul',
            ACL:    'public-read',
            keyTransform: function (relative_name) {
                return "images/" + relative_name;
            }
        }, {
            maxRetries: 5
        }));
    gulp.src("../services/public/font_awesome/fonts/*")
        .pipe(s3({
            Bucket: 'zona-azul',
            ACL:    'public-read',
            keyTransform: function (relative_name) {
                return "font_awesome/fonts/" + relative_name;
            }
        }, {
            maxRetries: 5
        }));
    gulp.src("../services/public/font_awesome/css/*")
        .pipe(s3({
            Bucket: 'zona-azul',
            ACL:    'public-read',
            keyTransform: function (relative_name) {
                return "font_awesome/css/" + relative_name;
            }
        }, {
            maxRetries: 5
        }));
    gulp.src("../services/public/material-design-iconic-font/fonts/*")
        .pipe(s3({
            Bucket: 'zona-azul',
            ACL:    'public-read',
            keyTransform: function (relative_name) {
                return "material-design-iconic-font/fonts/" + relative_name;
            }
        }, {
            maxRetries: 5
        }));
    gulp.src("../services/public/material-design-iconic-font/css/*")
        .pipe(s3({
            Bucket: 'zona-azul',
            ACL:    'public-read',
            keyTransform: function (relative_name) {
                return "material-design-iconic-font/css/" + relative_name;
            }
        }, {
            maxRetries: 5
        }));
    gulp.src("../services/public/ionicons/fonts/*")
        .pipe(s3({
            Bucket: 'zona-azul',
            ACL:    'public-read',
            keyTransform: function (relative_name) {
                return "ionicons/fonts/" + relative_name;
            }
        }, {
            maxRetries: 5
        }));
    gulp.src("../services/public/ionicons/css/*")
        .pipe(s3({
            Bucket: 'zona-azul',
            ACL:    'public-read',
            keyTransform: function (relative_name) {
                return "ionicons/css/" + relative_name;
            }
        }, {
            maxRetries: 5
        }));
    gulp.src(uploadFiles)
        .pipe(s3({
            Bucket: 'zona-azul',
            ACL:    'public-read'
        }, {
            maxRetries: 5
        }));
});

gulp.task('replace-assets', function () {
    gulp.src('./dist/index.html')
        .pipe(replace('/app.min.css', s3Host + 'app.min.css'))
        .pipe(replace('/app.min.js', s3Host + 'app.min.js'))
        .pipe(gulp.dest('./dist'));
    gulp.src('./dist/app.min.css')
        .pipe(replace('/images', '/zona-azul/images'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('replace-assets-hlg', function () {
    gulp.src('./dist/app.min.css')
        .pipe(replace('/images', '/zona-azul/images'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('closure', function() {
  return gulp.src('dist/app.js')
    .pipe(closureCompiler({
        compilerPath: 'closure-compiler.jar',
        fileName: 'dist/app.min.js'
    }))
    .pipe(gulp.dest('dist'));
});

function compileJs(compressed) {
    if (compressed) {
        return browserify({
            entries: './src/js/app.js',
            extensions: ['js']
        })
            .transform('babelify', {presets: ['es2015', 'react']})
            .bundle()
            .pipe(source('app.js'))
            .pipe(gulp.dest('./dist'))
            .pipe(connect.reload());
    } else {
        return browserify({
            entries: './src/js/app.js',
            extensions: ['js'],
            debug: true
        })
            .transform('babelify', {presets: ['es2015', 'react']})
            .bundle()
            .pipe(source('app.min.js'))
            .pipe(gulp.dest('./dist'))
            .pipe(connect.reload());
    }
}

gulp.task('js', function () {
    compileJs(false);
});

gulp.task('style', function () {
    var vendorStyle = gulp.src([
        './bower_components/OnsenUI/css/onsenui.css',
        './bower_components/OnsenUI/css/onsen-css-components.css',
        './bower_components/OnsenUI/css/onsen-css-components-blue-basic-theme.css',
        './node_modules/react-datepicker/dist/react-datepicker.css',
    ]);
    var appStyle = gulp.src('./src/scss/index.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: [
                './node_modules/compass-mixins/lib'
            ],
        }).on('error', sass.logError));
    return merge(vendorStyle, appStyle)
        .pipe(concat('app.min.css'))
        .pipe(uglifycss())
        .pipe(gulp.dest('./dist'))
        .pipe(connect.reload());
});

gulp.task('html', function () {
    return gulp.src('./src/index.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('./dist'))
        .pipe(connect.reload());
});

gulp.task('assets', function () {
    gulp.src('./bower_components/OnsenUI/css/font_awesome/**/*')
        .pipe(gulp.dest('./dist/font_awesome'));
    gulp.src('./bower_components/OnsenUI/css/ionicons/**/*')
        .pipe(gulp.dest('./dist/ionicons'));
    gulp.src('./bower_components/OnsenUI/css/material-design-iconic-font/**/*')
        .pipe(gulp.dest('./dist/material-design-iconic-font'));
    gulp.src('./src/images/*')
        .pipe(gulp.dest('./dist/images'));
});

gulp.task('build', ['style', 'html', 'assets'], function () {
    compileJs(true);
});

gulp.task('move-to-server', function () {
    gulp.src('./dist/**/**', {
        base: 'dist'
    })
        .pipe(gulp.dest(serverPublicFolder));

    gulp.src('./dist/index.html')
        .pipe(rename('web-app.blade.php'))
        .pipe(gulp.dest(serverIndexFolder));
});

gulp.task('remove-html-server', function () {
    gulp.src(serverPublicFolder + '/index.html')
        .pipe(clean({force: true}));
});

gulp.task('watch', function () {
    gulp.watch(['./src/js/**/*.js', './src/js/*.js'], ['js']);
    gulp.watch(['./src/scss/**/*.scss', './src/scss/*.scss'], ['style']);
    gulp.watch('./src/index.html', ['html']);
});

gulp.task('openbrowser', function() {
    opn( 'http://' + server.host + ':' + server.port + '/?code=LIB0101A');
});

gulp.task('connect', function() {
    connect.server({
        root: 'dist',
        livereload: true,
        host: server.host,
        port: server.port
    });
});

gulp.task('default', ['connect', 'watch', 'openbrowser']);
