const pages = {
    type: {
        id: 'type',
        title: 'Quantidade de CAD'
    },
    checkin: {
        id: 'checkin',
        title: 'Entrada'
    },
    checkout: {
        id: 'checkout',
        title: 'Saída'
    },
    plate: {
        id: 'plate',
        title: 'Placa'
    },
    payment: {
        id: 'payment',
        title: 'Forma de Pagamento'
    },
    receipt: {
        id: 'receipt',
        title: 'Comprovante de pagamento'
    },
    receipts: {
        id: 'receipts',
        title: 'Minhas Compras'
    },
    error: {
        id: 'error',
        title: 'Erro ao efetuar compra'
    },
    login: {
        id: 'login',
        title: 'Login'
    },
    recover: {
        id: 'recover',
        title: 'Recuperar Senha'
    },
    account: {
        id: 'account',
        title: 'Meus dados'
    },
    activeParking: {
        id: 'activeParking',
        title: 'Vaga'
    },
    profile: {
        id: 'profile',
        title: 'Perfil'
    },
    scheduleds: {
        id: 'scheduleds',
        title: 'Vagas Agendadas'
    },
    actives: {
        id: 'actives',
        title: 'Vagas Ativas'
    },
    rule: {
        id: 'rule',
        title: 'Tipo de CAD'
    },
    about: {
        id: 'about',
        title: 'Sobre Zona Azul Digital'
    }
};

export default pages;
