const actions = {
    setStreet: 'SET_STREET',
    setCheckin: 'SET_CHECKIN',
    setCheckout: 'SET_CHECKOUT',
    setType: 'SET_TYPE',
    setTypes: 'SET_TYPES',
    setPlate: 'SET_PLATE',
    setTicket: 'SET_TICKET',
    setTicketOptions: 'SET_TICKET_OPTIONS',
    setPaymentData: 'SET_PAYMENT_DATA',
    setPaymentMethod: 'SET_PAYMENT_METHOD',
    setPaymentOptions: 'SET_PAYMENT_OPTIONS',
    setUserEmail: 'SET_USER_EMAIL',
    setUserPassword: 'SET_USER_PASSWORD',
    setUserDocument: 'SET_USER_DOCUMENT',
    setUserDocumentType: 'SET_USER_DOCUMENT_TYPE',
    setUserBirthDate: 'SET_USER_BIRTHDATE',
    setUserData: 'SET_USER_DATA',
    setSending: 'SET_SENDING'
}

export default actions;
