import Env from '../utils/Env';

const months = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
    "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
const monthsShort = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago",
    "Set", "Out", "Nov", "Dez"];
const filterLabels = [
    {
        key: 'today',
        text: 'Hoje'
    }, {
        key: 'yesterday',
        text: 'Ontem'
    }, {
        key: 'last_3',
        text: '3 Dias'
    }, {
        key: 'last_7',
        text: '7 Dias'
    }, {
        key: 'last_30',
        text: '30 Dias'
    }, {
        key: 'month',
        text: 'Este Mês'
    }, {
        key: 'last_month',
        text: 'Mês Passado'
    }, {
        key: 'year',
        text: '12 Meses'
    }, {
        key: 'all',
        text: 'Todo o Período'
    }
];

let api_url;
let server_url;

switch (Env.get()) {
    case 'dev':
        api_url = '//parkme.api.app/api';
        server_url = '//parkme.api.app/';
        break;
    case 'homolog':
        api_url = '//parkme.coffeecupdev.com/api';
        server_url = '//parkme.coffeecupdev.com/';
        break;
    case 'prod':
        api_url = 'https://parkme.com.br/api';
        server_url = 'https://parkme.com.br/';
        break;
}

export default {
    api_url,
    server_url,
    months,
    monthsShort,
    filterLabels
}
