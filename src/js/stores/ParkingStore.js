import dispatcher from "../dispatcher";
import Request from "../utils/Request";
import Constants from "../constants/ActionsConstants";
import Base from "./BaseStore";

class ParkingStore extends Base {

    constructor() {
        super();
        this.street = null;
        this.options = null;
        this.ticketOptions = [];
        this.types = [];
        this.rules = [];
        this.ticket = null;
        this.type = null;
        this.rule = null;
        this.checkin = null;
        this.extensionId = null;
        this.hours = null;
        this.plate = null;
        this.sending = false;
        this.sendingStatus = "Enviando Compra";
        this.sendingFail = false;
        this.sendingSuccess = false;
        this.morning = false;
        this.morningTime = 'now';
        this.hoursChanged = false;
        this.cancelAnother = false;
    }

    getCancelAnother() {
        return this.cancelAnother;
    }

    setCancelAnother(cancelAnother) {
        this.cancelAnother = cancelAnother;
        this.change('cancel_another');
    }

    getHoursChanged() {
        return this.hoursChanged;
    }

    setHoursChanged(hoursChanged) {
        this.hoursChanged = hoursChanged;
        this.change('hours_changed');
    }

    getMorning() {
        return this.morning;
    }

    setMorning(morning) {
        this.morning = morning;
        this.change('morning');
    }

    getMorningTime() {
        return this.morningTime;
    }

    setMorningTime(morningTime) {
        this.morningTime = morningTime;
        this.change('morning_time');
    }

    getExtensao() {
        return this.extensionId;
    }

    setExtensao(extensao) {
        this.extensionId = extensao;
        this.change('extensao');
    }

    getStreet() {
        return this.street;
    }

    setStreet(street) {
        this.street = street;
        this.change('street');
    }

    getTicket() {
        return this.ticket;
    }

    setTicket(ticket) {
        this.ticket = ticket;

        this.resetHours();

        this.change('ticket');
    }

    getTicketOptions() {
        return this.ticketOptions;
    }

    setTicketOptions(ticketOptions) {
        this.ticketOptions = ticketOptions;
        this.ticket = this.ticketOptions[0];
        this.resetHours();
        this.change('ticketOptions');
    }

    getType() {
        return this.type;
    }

    setType(type) {
        this.type = type;

        if (type.prices && type.prices.length > 0) {
            this.options = this.ticketOptions;
            this.setTicketOptions(type.prices);
        } else if (this.options && this.options != this.ticketOptions) {
            this.setTicketOptions(this.options);
        }

        this.change('type');
    }

    getRule() {
        return this.rule;
    }

    setRule(rule) {
        this.rule = rule;
        this.change('rule');

        // activating truck when rule 1 is selected
        if (this.rule.number == 1) {
            for (var i in this.types) {
                if (this.types[i].slug == 'caminhao') {
                    this.setType(this.types[i]);
                    break;
                }
            }
        }

        if (this.checkin) {
            this.resetCheckout(this.checkin);
        }
    }

    getRules() {
        return this.rules;
    }

    setRules(rules) {
        this.rules = rules;

        if (this.rules && this.rules.length > 0) {
            this.setRule(this.rules[0]);
        }

        this.change('rules');
    }

    getTypes() {
        return this.types;
    }

    setTypes(types) {
        this.types = types;
        this.type = this.types[0];
        this.change('types');
    }

    resetHours() {
        if (this.ticket.pre_charger == 1) {
            // não permitir compra pré-paga e ativação
            this.setHours({
                id: 0,
                amount: 0,
                title: 'Não ativar'
            });
            // this.setHours(this.ticketOptions[0]);
        } else {
            this.setHours(this.ticket);
        }

        if (this.checkin) {
            this.resetCheckout(this.checkin);
        }
    }

    getHours() {
        return this.hours;
    }

    setHours(hours) {
        this.hours = hours;
        this.change('hours');

        if (this.checkin) {
            this.resetCheckout(this.checkin);
        }
    }

    resetCheckin(checkout) {
        this.checkin = new Date(checkout.getTime());
        let minutes = this.rule ? this.rule.time : 0;

        if (this.ticket && this.ticket.pre_charger != 1) {
            minutes*= this.ticket.amount;
        }

        this.checkin.setMinutes(this.checkin.getMinutes() - minutes);
        this.change('checkin');
    }

    getCheckin() {
        return this.checkin;
    }

    setCheckin(time) {
        this.checkin = time;
        this.resetCheckout(this.checkin);
        this.change('checkin');
    }

    resetCheckout(checkin) {
        this.checkout = new Date(checkin.getTime());
        let minutes = this.rule ? this.rule.time : 0;

        if (this.ticket && this.ticket.pre_charger != 1) {
            minutes*= this.ticket.amount;
        }

        this.checkout.setMinutes(this.checkout.getMinutes() + minutes);
        this.change('checkout');
    }

    getCheckout() {
        return this.checkout;
    }

    setCheckout(time) {
        this.checkout = time;
        this.resetCheckin(time);
        this.change('checkout');
    }

    getSending() {
        return this.sending;
    }

    setSending(sending) {
        this.sending = sending;
        this.change('sending');
    }

    getSendingStatus() {
        return this.sendingStatus;
    }

    setSendingStatus(sendingStatus) {
        this.sendingStatus = sendingStatus;
        this.change('sending_status');
    }

    getSendingFail() {
        return this.sendingFail;
    }

    setSendingFail(sendingFail) {
        this.sendingFail = sendingFail;
        this.change('sending_fail');
    }

    getSendingSuccess() {
        return this.sendingSuccess;
    }

    setSendingSuccess(sendingSuccess) {
        this.sendingSuccess = sendingSuccess;
        this.change('sending_success');
    }

    handleActions(action) {
        switch (action.type) {
            case Constants.setStreet:
                this.setStreet(action.street);
                break;
            case Constants.setCheckout:
                this.setCheckout(action.time);
                break;
            case Constants.setCheckin:
                this.setCheckin(action.time);
                break;
            case Constants.setType:
                this.setType(action.data);
                break;
            case Constants.setTypes:
                this.setTypes(action.data);
                break;
            case Constants.setTicket:
                this.setTicket(action.ticket);
                break;
            case Constants.setTicketOptions:
                this.setTicketOptions(action.ticketOptions);
                break;
            case Constants.setSending:
                this.setSending(action.sending);
                break;
        }
    }
}

const store = new ParkingStore;
dispatcher.register(store.handleActions.bind(store));

export default store;
