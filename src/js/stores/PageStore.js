import dispatcher from "../dispatcher";
import Request from "../utils/Request";
import Constants from "../constants/ActionsConstants";
import Base from "./BaseStore";

class PageStore extends Base {
    constructor() {
        super();
        this.name = 'order';
        this.generateGettersAndSetters();
    }

    fields() {
        return ['name', 'title'];
    }

    goHome() {
        this.emit('go_home');
    }

    handleActions(action) {

    }
}

const store = new PageStore;
dispatcher.register(store.handleActions.bind(store));

export default store;
