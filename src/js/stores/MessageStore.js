import dispatcher from "../dispatcher";
import Base from "./BaseStore";

class MessageStore extends Base {
    constructor() {
        super();
        this.messages = {
            title: null,
            text: null,
            alert: null,
            yes: null,
            no: null
        };
        this.isAlert = false;
        this.isConfirm = false;
        this.showing = false;
    }

    get(type) {
        if (this.messages[type] !== undefined) {
            return this.messages[type];
        }

        return false;
    }

    set(type, message, noEmit) {
        if (this.messages[type] !== undefined) {
            this.messages[type] = message;
            if (!noEmit) {
                this.emit('message_'+type);
            }
        }
    }

    setAsAlert() {
        this.isAlert = true;
        this.isConfirm = false;
        this.emit('type');
    }

    setAsConfirm() {
        this.isAlert = false;
        this.isConfirm = true;
        this.emit('type');
    }

    setNoButton() {
        this.isAlert = false;
        this.isConfirm = false;
        this.emit('type');
    }

    show() {
        this.showing = true;
        this.emit('showing');
        this.emit('show');
    }

    hide() {
        this.showing = false;
        this.emit('showing');
        this.emit('hide');
    }

    alert() {
        this.emit('alert');
    }

    yes() {
        this.emit('yes');
    }

    no() {
        this.emit('no');
    }

    handleActions() {

    }
}

const store = new MessageStore;
dispatcher.register(store.handleActions.bind(store));

export default store;
