import dispatcher from "../dispatcher";
import Request from "../utils/Request";
import Constants from "../constants/ActionsConstants";
import Base from "./BaseStore";

class UserStore extends Base {
    constructor() {
        super();
        this.needFields = [];
        this.generateGettersAndSetters();
        this.accept_notifications = true;
        this.plate_id = null;
        this.plates = [];
        this.cards = [];
        this.orders = [];
        this.actives = [];
        this.activeParking = null;
        this.scheduleds = [];
        this.order = null;
    }

    fields() {
        return ['id', 'email', 'name', 'last_name', 'accept_notifications',
            'document', 'phone_number', 'plate', 'device_id', 'credits',
            'terms', 'password', 'guest'];
    }

    setPlate(plate) {
        this.plate = plate;
        this.plate_id = null;
        this.change('plate');
    }

    getPlateId() {
        return this.plate_id;
    }

    setPlateId(id) {
        this.plate_id = id;
        this.change('plate_id');
    }

    getPlates() {
        return this.plates;
    }

    setPlates(plates) {
        this.plates = plates;
        this.change('plates');
    }

    getCards() {
        return this.cards;
    }

    setCards(cards) {
        this.cards = cards;
        this.change('cards');
    }

    setNeedFields(needFields) {
        this.needFields = needFields;
        this.change('needs');
    }

    getNeedFields() {
        return this.needFields;
    }

    setOrders(orders) {
        this.orders = orders;
        this.change('orders');
    }

    getOrders() {
        return this.orders;
    }

    setActives(actives) {
        this.actives = actives;
        this.change('actives');
    }

    getActives() {
        return this.actives;
    }

    setActiveParking(activeParking) {
        this.activeParking = activeParking;
        this.change('active_parking');
    }

    getActiveParking() {
        return this.activeParking;
    }

    setActiveParkingId(activeParkingId) {
        this.activeParkingId = activeParkingId;
        this.change('active_parking_id');
    }

    getActiveParkingId() {
        return this.activeParkingId;
    }

    setScheduleds(scheduleds) {
        this.scheduleds = scheduleds;
        this.change('scheduleds');
    }

    getScheduleds() {
        return this.scheduleds;
    }

    setOrder(order) {
        this.order = order;
        this.change('order');
    }

    getOrder() {
        return this.order;
    }

    handleActions(action) {
        switch (action.type) {
            case Constants.setUserEmail:
                this.setEmail(action.email);
                break;
            case Constants.setUserPassword:
                this.setPassword(action.password);
                break;
            case Constants.setUserDocument:
                this.setDocument(action.document);
                break;
            case Constants.setUserDocumentType:
                this.setDocumentType(action.documentType);
                break;
            case Constants.setUserBirthDate:
                this.setBirthDate(action.date);
                break;
            case Constants.setPlate:
                this.setPlate(action.plate);
                break;
        }
    }
}

const store = new UserStore;
dispatcher.register(store.handleActions.bind(store));

export default store;
