import dispatcher from "../dispatcher";
import Request from "../utils/Request";
import Constants from "../constants/ActionsConstants";
import Base from "./BaseStore";

class PaymentStore extends Base {
    constructor() {
        super();
        this.order_id = null;
        this.id = null;
        this.type = null;
        this.last_digit = null;
        this.name = null;
        this.options = [];
        this.method = null;
        this.data = {};
        this.discount = 0;
    }

    getOrderId() {
        return this.order_id;
    }

    setOrderId(order_id) {
        this.order_id = order_id;
        this.change('order_id');
    }

    getId() {
        return this.id;
    }

    setId(id) {
        this.id = id;
        this.change('id');
    }

    getType() {
        return this.type;
    }

    setType(type) {
        this.type = type;
        this.change('type');
    }

    getLastDigit() {
        return this.last_digit;
    }

    setLastDigit(last_digit) {
        this.last_digit = last_digit;
        this.change('last_digit');
    }

    getName() {
        return this.name;
    }

    setName(name) {
        this.name = name;
        this.change('name');
    }

    getOptions() {
        return this.options;
    }

    setOptions(options) {
        this.options = options;

        this.setMethod(this.options[0]);

        this.change('options');
    }

    getMethod() {
        return this.method;
    }

    setMethod(method) {
        this.method = method;
        this.change('method');
    }

    getData() {
        return this.data;
    }

    setData(data) {
        this.data = data;
        this.change('data');
    }

    getDiscount() {
        return this.discount;
    }

    setDiscount(discount) {
        this.discount = discount;
        this.change('discount');
    }

    handleActions(action) {
        switch (action.type) {
            case Constants.setPaymentMethod:
                this.setMethod(action.method);
                break;
            case Constants.setPaymentData:
                this.setData(action.data);
                break;
            case Constants.setPaymentOptions:
                this.setOptions(action.options);
                break;
        }
    }
}

const store = new PaymentStore;
dispatcher.register(store.handleActions.bind(store));

export default store;
