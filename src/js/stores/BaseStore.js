import { EventEmitter } from "events";

import String from '../utils/String';

export default class BaseStore extends EventEmitter {
    constructor() {
        super();
    }

    change(type) {
        this.emit('change_' + type);
        this.emit('change');
    }

    generateGettersAndSetters() {
        const fields = this.fields();
        let field;
        for (var f in fields) {
            this[fields[f]] = this[fields[f]] || '';
            this.generateGetter(fields[f]);
            this.generateSetter(fields[f]);
        }
    }

    generateGetter(field) {
        const me = this;
        const snakeField = String.ucFirst(String.snakeCase(field));
        if (!me['get' + snakeField]) {
            me['get' + snakeField] = () => {
                return me[field];
            }
        }
    }

    generateSetter(field) {
        const me = this;
        const snakeField = String.ucFirst(String.snakeCase(field));
        if (!me['set' + snakeField]) {
            me['set' + snakeField] = (value) => {
                me[field] = value;
                me.change(field);
            }
        }
    }

    setData(fill) {
        const me = this;
        const fields = me.fields();

        for (var f in fields) {
            if (fill[fields[f]]) {
                me[fields[f]] = fill[fields[f]];
            }
        }

        this.emit('change');
    }

    getData() {
        const me = this;
        const data = {};
        const fields = this.fields();

        for (var f in fields) {
            data[fields[f]] = me[fields[f]];
        }

        return data;
    }
}
