import React from "react";

import Store from '../../stores/MessageStore';
import Message from './Message';

export default class Title extends React.Component {
    constructor() {
        super();
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            text: Store.get('title')
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('message_title', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('message_title', this.updateState);
    }

    render() {
        const { text } = this.state;
        let html = null;

        if (text) {
            html = <div className='alert-dialog-title'>{text}</div>
        }

        return html;
    }
}
