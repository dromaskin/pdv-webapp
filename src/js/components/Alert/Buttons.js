import React from "react";

import Store from '../../stores/MessageStore';
import Message from './Message';

export default class Buttons extends React.Component {
    constructor() {
        super();
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            isAlert: Store.isAlert,
            isConfirm: Store.isConfirm,
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('type', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('type', this.updateState);
    }

    handleAlertClick() {
        Store.alert();
    }

    handleYesClick() {
        Store.yes();
    }

    handleNoClick() {
        Store.no();
    }

    render() {
        const { isAlert, isConfirm } = this.state;
        let buttons = [];

        if (isAlert) {
            buttons.push(<button
                    key='alert'
                    onClick={this.handleAlertClick.bind(this)}
                    className='alert-dialog-button alert-dialog-button--one'>
                    <Message text_key='alert' />
                </button>);
        } else if (isConfirm) {
            buttons.push(<button
                    key='no'
                    onClick={this.handleNoClick.bind(this)}
                    className='alert-dialog-button alert-dialog-button--one'>
                    <Message text_key='no' />
                </button>);
            buttons.push(<button
                    key='yes'
                    onClick={this.handleYesClick.bind(this)}
                    className='alert-dialog-button alert-dialog-button--one'>
                    <Message text_key='yes' />
                </button>);
        }

        return (
            <div className='alert-dialog-footer alert-dialog-footer--one'>
                {buttons}
            </div>
        );
    }
}
