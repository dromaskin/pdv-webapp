import React from "react";

import Store from '../../stores/MessageStore';

export default class Message extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState(props);
    }

    initialState(props) {
        let key = 'text';

        if (props && props.text_key) {
            key = props.text_key;
        }

        if (this.state && this.state.text_key) {
            key = this.state.text_key
        }

        return {
            text_key: key,
            message: Store.get(key)
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('message_' + this.state.text_key, this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('message_' + this.state.text_key, this.updateState);
    }

    render() {
        const { message } = this.state;
        const html = {
            __html: message
        }

        return (
            <span dangerouslySetInnerHTML={html}></span>
        );
    }
}
