import React from "react";
import { Dialog, AlertDialog } from 'react-onsenui';

import Store from '../stores/MessageStore';
import Message from './Alert/Message';
import Buttons from './Alert/Buttons';
import Title from './Alert/Title';

export default class Dialog extends React.Component {
    constructor() {
        super();
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    render() {
        const {
            show
        } = this.state;

        return (
            <Dialog
                isOpen={show}
                isCancelable={false}>
                <div>

                </div>
            </Dialog>
        );
    }
}
