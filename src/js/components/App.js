import React from "react";
import Order from "../pages/Order";
import { Router, Route, browserHistory } from 'react-router';
import LoadData from '../requests/LoadData';
import StoreData from '../requests/StoreData';
import Loading from '../pages/Loading';
import Error from '../pages/Error';
import Moment from 'moment';
import Ons from 'onsenui';
import 'moment/locale/pt-br';

Ons.disableAutoStyling();

export default class App extends React.Component {
    constructor(props) {
        super(props);

        Moment.locale('pt-BR');

        this.state = {
            loaded: false,
            data: false
        }

        const me = this;

        LoadData.parking(function(response) {
            if (response && response.status && response.data) {
                StoreData(response.data);
                me.setState({
                    data: {
                        status: true
                    },
                    loaded: true
                });
            } else {
                me.setState({
                    data: response,
                    loaded: true
                });
            }
        });
    }

    render() {
        const { loaded, data } = this.state;
        let view;

        if (!loaded) {
            view = <Loading />;
        } else if(data && data.status) {
            view = <Order />;
        } else {
            const message = data.message || "Erro ao consultar dados da localização. Tente novamente";
            view = <Error message={message} />;
        }

        return view;
    }
}
