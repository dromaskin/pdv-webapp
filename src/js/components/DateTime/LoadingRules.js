import React from 'react';

import CheckDateRule from '../../requests/CheckDateRule';
import MessageStore from '../../stores/MessageStore';

import { Button } from 'react-onsenui';

export default class LoadingRules extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: this.props.date,
            allowed: true,
            text: 'Carregando'
        };
        this.handleClose = this.handleClose.bind(this);
    }

    componentDidMount() {
        const me = this;

        CheckDateRule(this.props.date, (response) => {
            if (response.allowed) {
                this.props.handleClose();
            } else {
                MessageStore.set('text', response.message || 'Erro ao consultar horário da vaga');
                MessageStore.set('alert', 'Fechar');
                MessageStore.setAsAlert();
            }
        });
        MessageStore.on('alert', this.handleClose);
    }

    componentWillUnmount() {
        MessageStore.removeListener('alert', this.handleClose);
    }

    handleClose() {
        MessageStore.hide();
    }

    render() {
        const { text, allowed } = this.state;
        let button = '';

        if (!allowed) {
            button = <Button onClick={this.handleClose.bind(this)}>
                        Fechar
                    </Button>
        }

        return (
            <div className='dialog-content'>
                <p>{text}</p>
                {button}
            </div>
        );
    }
}
