import React from 'react';

import DatePicker from 'react-datepicker';
import Moment from 'moment';

import { Button } from 'react-onsenui';

export default class Calendar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            moment: new Moment(this.props.date)
        };
    }

    handleChange(moment) {
        this.setState({moment});
    }

    handleClose() {
        this.props.handleChange(this.state.moment.toDate());
    }

    render() {
        const { moment } = this.state;
        return (
            <div className='datepicker-wrapper'>
                <DatePicker
                    selected={moment}
                    inline
                    minDate={Moment()}
                    onChange={this.handleChange.bind(this)} />
                <Button
                    className='block text-center'
                    onClick={this.handleClose.bind(this)}>OK</Button>
            </div>
        );
    }
}
