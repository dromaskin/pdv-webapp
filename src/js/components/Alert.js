import React from "react";
import { AlertDialog } from 'react-onsenui';

import Store from '../stores/MessageStore';
import Message from './Alert/Message';
import Buttons from './Alert/Buttons';
import Title from './Alert/Title';

export default class Alert extends React.Component {
    constructor() {
        super();
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            show: Store.showing
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('showing', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('showing', this.updateState);
    }

    render() {
        const {
            show
        } = this.state;

        return (
            <AlertDialog
                isOpen={show}
                isCancelable={false}>
                <Title />
                <div className='alert-dialog-content'>
                    <Message />
                </div>
                <Buttons />
            </AlertDialog>
        );
    }
}
