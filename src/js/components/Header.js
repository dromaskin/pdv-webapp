import React from "react";

import ParkingStore from '../stores/ParkingStore';
import UserStore from '../stores/UserStore';
import PageStore from '../stores/PageStore';
import Logout from '../requests/Logout';

export default class Header extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
        this.state.disabled = props.disabled;
    }

    initialState() {
        return {
            street: ParkingStore.getStreet(),
            name: UserStore.getName(),
            id: UserStore.getId(),
            scheduleds: UserStore.getScheduleds(),
            actives: UserStore.getActives(),
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        ParkingStore.on('change_street', this.updateState);
        UserStore.on('change_name', this.updateState);
        UserStore.on('change_scheduleds', this.updateState);
        UserStore.on('change_actives', this.updateState);
        UserStore.on('change_id', this.updateState);
    }

    componentWillUnmount() {
        ParkingStore.removeListener('change_street', this.updateState);
        UserStore.removeListener('change_name', this.updateState);
        UserStore.removeListener('change_scheduleds', this.updateState);
        UserStore.removeListener('change_actives', this.updateState);
        UserStore.removeListener('change_id', this.updateState);
    }

    handleClick() {
        PageStore.setName('account');
    }

    handleLogout() {
        Logout();
    }

    handleClickActiveParkings() {
        if (this.state.actives.length > 1) {
            PageStore.setName('actives');
        } else {
            UserStore.setActiveParking(null);
            UserStore.setActiveParkingId(this.state.actives[0].order_id);
            PageStore.setName('activeParking');
        }
    }

    handleClickScheduleds() {
        PageStore.setName('scheduleds');
    }

    render() {
        const { street, name, id, disabled, scheduleds, actives } = this.state;
        let userText = '';
        let scheduledsText = '';
        let activesText = '';
        let link = <a
                    onClick={this.handleClick.bind(this)}
                    className="btn btn-outline btn-right">ver conta</a>;

        if (disabled == true) {
            link = '';
        }

        if (scheduleds.length > 0 && PageStore.getName() != 'scheduleds') {
            let scheduledsAmount = 0;
            scheduleds.map((scheduled) => {
                scheduledsAmount+= parseInt(scheduled.amount);
            });
            scheduledsText = <a
                onClick={this.handleClickScheduleds.bind(this)}
                className="btn btn-info">
                {scheduledsAmount} Agendado
                {scheduledsAmount > 1 ? "s" : ""}
            </a>;
        }

        if (actives.length > 0 &&
            ['activeParking', 'activeParkings'].indexOf(PageStore.getName()) < 0) {
            let activesAmount = 0;
            actives.map((active) => {
                activesAmount+= parseInt(active.amount);
            });
            activesText = <a
                onClick={this.handleClickActiveParkings.bind(this)}
                className="btn btn-danger">
                {activesAmount} Ativo
                {activesAmount > 1 ? "s" : ""}
            </a>;
        }

        if (name && id) {
            userText = <div>
                            Olá {name},&nbsp;
                            <a onClick={this.handleLogout.bind(this)} className="inline-link">Fazer logout</a>
                            <br />
                            {activesText}{scheduledsText}{link}
                        </div>;
        }

        return (
            <div className="header">
                <div className="street-info">
                    {/*street.name*/}
                    {userText}
                </div>
            </div>
        );
    }
}
