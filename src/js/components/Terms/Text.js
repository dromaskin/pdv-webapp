import React from "react";

import LoadTerms from '../../requests/LoadTerms';

export default class Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '<div class="text-center">Carregando</div>'
        }
    }

    componentDidMount() {
        const terms = document.querySelector('.dialog-container .terms');
        terms.setAttribute('style', 'height: auto');
        LoadTerms((response) => {
            if (response && response.data) {
                const height = window.innerHeight ||
                    document.documentElement.clientHeight ||
                    document.body.clientHeight;
                const containerHeight = (height*0.8)-80;
                terms.setAttribute('style', 'height: ' + containerHeight + 'px');
                this.setState({
                    text: response.data
                });
            } else {
                this.setState({
                    text: '<div class="text-center">Erro ao carregar termos de uso</div>'
                });
            }
        });
    }

    render() {
        const me = this;
        const { text } = this.state;
        const html = {
            __html: text
        }

        return (
            <div dangerouslySetInnerHTML={html}></div>
        );
    }
}
