import React from "react";
import { ListItem } from 'react-onsenui';

import Store from '../../stores/UserStore';
import PageStore from '../../stores/PageStore';

export default class Plate extends React.Component {
    constructor() {
        super();
        this.state = {
            plate: Store.getPlate()
        }
        this.getPlate = this.getPlate.bind(this);
    }

    getPlate() {
        this.setState({
            plate: Store.getPlate()
        });
    }

    componentWillMount() {
        Store.on('change_plate', this.getPlate);
    }

    componentWillUnmount() {
        Store.removeListener('change_plate', this.getPlate);
    }

    handleClick() {
        PageStore.setName('plate');
    }

    render() {
        const { plate } = this.state;
        const text = plate ? "Placa" : "Informe a placa";
        let detail = '';

        if (plate) {
            detail = <div className='right'>
                <div className="value">{plate}</div>
            </div>
        } else {
            detail = <div className='right'>
                <span className="link no-margin">
                    Adicionar Placa
                </span>
            </div>
        }

        return (
            <ListItem modifier={"chevron"} onClick={this.handleClick.bind(this)}>
                <div className='center'>
                    <div className="list__item__title title">{text}</div>
                </div>
                {detail}
            </ListItem>
        );
    }
}
