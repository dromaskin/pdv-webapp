import React from "react";
import { ListItem, Input } from 'react-onsenui';

import Store from '../../../stores/UserStore';
import Validate from '../../../utils/Validate';
import String from '../../../utils/String';

export default class PhoneNumber extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.formatPhoneNumber = this.formatPhoneNumber.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            phoneNumber: this.formatPhoneNumber(Store.getPhoneNumber())
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change_phone_number', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('change_phone_number', this.updateState);
    }

    formatPhoneNumber(numbers) {
        let phone = '';

        if (numbers.length == 11) {
            phone+= '(' + numbers.substring(0, 2) + ') ' +
                numbers.substring(2, 3) + ' ' + numbers.substring(3, 7) + '-' +
                numbers.substring(7, 11);
        } else if (numbers.length > 7) {
            phone+= '(' + numbers.substring(0, 2) + ') ' +
                numbers.substring(2, 3) + ' ' + numbers.substring(3, 7) +
                '-' + numbers.substring(7, 11);
        } else if (numbers.length > 3) {
            phone+= '(' + numbers.substring(0, 2) + ') ' +
                numbers.substring(2, 3) + ' ' + numbers.substring(3, 7);
        } else if (numbers.length > 2) {
            phone+= '(' + numbers.substring(0, 2) + ') ' +
                numbers.substring(2, 3);
        } else if (numbers.length > 0) {
            phone+= '(' + numbers.substring(0, 2);
        }

        return phone;
    }

    handleChange(e) {
        let numbers = String.numbers(e.target.value);
        Store.setPhoneNumber(numbers);
    }

    render() {
        const { phoneNumber } = this.state;

        return (
            <ListItem modifier='longdivider'>
                <div className="left">
                    Celular
                </div>
                <div className="center">
                    <Input
                        id='name'
                        onKeyUp={this.handleChange.bind(this)}
                        name='name'
                        maxLength={16}
                        pattern="[0-9]*"
                        placeholder='(XX) X XXXX XXXX'
                        className='block'
                        value={phoneNumber} />
                </div>
            </ListItem>
        );
    }
}
