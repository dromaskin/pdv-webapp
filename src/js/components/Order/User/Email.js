import React from "react";
import { ListItem, Input } from 'react-onsenui';

import Store from '../../../stores/UserStore';
import Validate from '../../../utils/Validate';
import * as Actions from '../../../actions/UserActions';

export default class Email extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            email: Store.getEmail()
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change_email', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('change_email', this.updateState);
    }

    handleChange(e) {
        Actions.setEmail(e.target.value);
    }

    render() {
        const { email } = this.state;
        return (
            <ListItem modifier='longdivider'>
                <div className="left">
                    Email
                </div>
                <div className="center">
                    <Input
                        onChange={this.handleChange.bind(this)}
                        type='email'
                        name='email'
                        placeholder='Email'
                        className='block'
                        value={email} />
                </div>
            </ListItem>
        );
    }
}
