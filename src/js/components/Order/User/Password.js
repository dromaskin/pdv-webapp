import React from "react";
import { ListItem, Input } from 'react-onsenui';

import Store from '../../../stores/UserStore';
import Validate from '../../../utils/Validate';

export default class Password extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            password: Store.getPassword()
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change_password', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('change_password', this.updateState);
    }

    handleChange(e) {
        Store.setPassword(e.target.value);
    }

    render() {
        const { password } = this.state;

        return (
            <ListItem modifier='longdivider'>
                <div className="left">
                    Senha
                </div>
                <div className="center">
                    <Input
                        type='password'
                        id='password'
                        onKeyUp={this.handleChange.bind(this)}
                        name='password'
                        placeholder='Senha'
                        className='block'
                        value={password} />
                </div>
            </ListItem>
        );
    }
}
