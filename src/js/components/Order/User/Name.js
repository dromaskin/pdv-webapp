import React from "react";
import { ListItem, Input } from 'react-onsenui';

import Store from '../../../stores/UserStore';
import Validate from '../../../utils/Validate';

export default class Name extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            name: Store.getName()
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change_name', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('change_name', this.updateState);
    }

    handleChange(e) {
        Store.setName(e.target.value);
    }

    render() {
        const { name } = this.state;

        return (
            <ListItem modifier='longdivider'>
                <div className="left">
                    Nome
                </div>
                <div className="center">
                    <Input
                        id='name'
                        onKeyUp={this.handleChange.bind(this)}
                        name='name'
                        placeholder='Nome'
                        className='block'
                        value={name} />
                </div>
            </ListItem>
        );
    }
}
