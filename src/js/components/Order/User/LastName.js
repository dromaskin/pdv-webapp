import React from "react";
import { ListItem, Input } from 'react-onsenui';

import Store from '../../../stores/UserStore';
import Validate from '../../../utils/Validate';

export default class LastName extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            lastName: Store.getLastName()
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change_last_name', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('change_last_name', this.updateState);
    }

    handleChange(e) {
        Store.setLastName(e.target.value);
    }

    render() {
        const { lastName } = this.state;

        return (
            <ListItem modifier='longdivider'>
                <div className="left">
                    Sobrenome
                </div>
                <div className="center">
                    <Input
                        id='name'
                        onKeyUp={this.handleChange.bind(this)}
                        name='name'
                        placeholder='Sobrenome'
                        className='block'
                        value={lastName} />
                </div>
            </ListItem>
        );
    }
}
