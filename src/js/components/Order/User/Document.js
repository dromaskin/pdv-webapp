import React from "react";
import { ListItem, Input } from 'react-onsenui';

import Store from '../../../stores/UserStore';
import Validate from '../../../utils/Validate';
import * as Actions from '../../../actions/UserActions';
import String from '../../../utils/String';

export default class Document extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            document: String.formatDocument(Store.getDocument())
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change_document', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('change_document', this.updateState);
    }

    handleChange(e) {
        let numbers = String.numbers(e.target.value);

        Actions.setDocument(numbers);
    }

    render() {
        const { document } = this.state;

        return (
            <ListItem modifier='longdivider'>
                <div className="left">
                    CPF/CNPJ
                </div>
                <div className="center">
                    <Input
                        id='user-document'
                        onKeyUp={this.handleChange.bind(this)}
                        name='document'
                        maxlength={18}
                        pattern="[0-9]*"
                        placeholder='Número do documento'
                        className='block'
                        value={document} />
                </div>
            </ListItem>
        );
    }
}
