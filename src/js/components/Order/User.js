import React from "react";
import { List, ListHeader, Button } from 'react-onsenui';

import Email from './User/Email';
import Document from './User/Document';
import Name from './User/Name';
import LastName from './User/LastName';
import PhoneNumber from './User/PhoneNumber';
import Password from './User/Password';
import Terms from './Terms';
import AcceptNotifications from './AcceptNotifications';
import TermsDialog from '../../pages/Terms';
import Store from "../../stores/UserStore";
import MessageStore from "../../stores/MessageStore";
import PageStore from '../../stores/PageStore';
import Validate from '../../utils/Validate';
import CreateUser from '../../requests/CreateUser';

export default class User extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.closeAlert = this.closeAlert.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        let showTerms = false;
        let sending = false;

        if (this.state && typeof this.state.showTerms == 'boolean') {
            showTerms = this.state.showTerms;
        }

        if (this.state && typeof this.state.sending == 'boolean') {
            sending = this.state.sending;
        }

        return {
            data: Store.getData(),
            fields: Store.getNeedFields(),
            showTerms: showTerms,
            sending: sending
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    closeAlert() {
        MessageStore.hide();
    }

    componentWillMount() {
        Store.on('change', this.updateState);
        MessageStore.on('alert', this.closeAlert);
    }

    componentWillUnmount() {
        Store.removeListener('change', this.updateState);
        MessageStore.removeListener('alert', this.closeAlert);
    }

    handleLogin() {
        PageStore.setName('login');
    }

    handleClick() {
        const me = this;
        me.setState({
            sending: true
        });
        CreateUser((response) => {
            me.setState({
                sending: false
            });
            if (!(response && response.status)) {
                let message = 'Erro ao criar usuário';
                if (response.message) {
                    if (typeof response.message == 'object') {
                        message = response.message.join('<br />');
                    }
                }

                MessageStore.set('text', message);
                MessageStore.set('alert', 'Fechar');
                MessageStore.set('title', 'Alerta');
                MessageStore.setAsAlert();
                MessageStore.show();
            }
        });
    }

    handleCloseTerms() {
        this.setState({
            showTerms: false
        });
    }

    handleOpenTerms() {
        this.setState({
            showTerms: true
        });
    }

    renderRow(row, index) {
        switch (row) {
            case 'email':
                return <Email key='user-email' />;
            case 'document':
                return <Document key='user-document' />;
            case 'name':
                return <Name key='name' />;
            case 'last_name':
                return <LastName key='last-name' />;
            case 'phone_number':
                return <PhoneNumber key='phone-number' />;
            case 'password':
                return <Password key='password' />;
            case 'accept_notifications':
                return <AcceptNotifications key='accept-notifications' />;
            case 'terms':
                return <Terms
                        onOpen={this.handleOpenTerms.bind(this)}
                        key={"terms"} />;
            default:
                return '';
        }
    }

    render() {
        const { data, fields, showTerms, sending } = this.state;
        let form = null;
        // const valid = Validate.user(data);
        const valid = true;

        if (fields.length > 0) {
            if (fields.indexOf('password') < 0) {
                fields.splice(fields.length-2,0,'password');
            }

            // const showLogin = fields.indexOf('name') >= 0;
            const showLogin = true;

            form = <div>
                    <List
                        dataSource={fields}
                        renderRow={this.renderRow.bind(this)}
                        renderHeader={() => <ListHeader>Cadastro de usuário</ListHeader>} />
                    {showLogin ? <span
                        className="link login text-center"
                        onClick={this.handleLogin.bind(this)}>
                        Já possui conta? Faça login.
                    </span> : ''}
                    <Button
                        className="block text-center uppercase"
                        disabled={!valid || sending}
                        onClick={this.handleClick.bind(this)}>
                        {!sending ? 'Cadastrar' : 'Enviando'}
                    </Button>
                    <TermsDialog
                        show={showTerms}
                        onClose={this.handleCloseTerms.bind(this)} />
                </div>
        }

        return form;
    }
}
