import React from 'react';
import { Button } from 'react-onsenui';

import Store from '../../stores/ParkingStore';
import PageStore from '../../stores/PageStore';

export default class SendingStatus extends React.Component {
    constructor() {
        super();
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            status: Store.getSendingStatus(),
            fail: Store.getSendingFail(),
            success: Store.getSendingSuccess()
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change_sending_status', this.updateState);
        Store.on('change_sending_fail', this.updateState);
        Store.on('change_sending_success', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('change_sending_status', this.updateState);
        Store.removeListener('change_sending_fail', this.updateState);
        Store.removeListener('change_sending_success', this.updateState);
    }

    handleSuccess() {
        this.props.onClose();
        PageStore.setName('receipt');
    }

    render() {
        const { status, fail, success } = this.state;
        const html = {
            __html: status
        }
        let buttonFail = '';
        let buttonSuccess = '';
        if (fail) {
            buttonFail = <div>
                        <Button
                            onClick={this.props.onClose}>
                            Fechar
                        </Button>
                    </div>
        }
        if (success) {
            buttonSuccess = <div>
                        <Button
                            onClick={this.handleSuccess.bind(this)}>
                            Comprovante de pagamento
                        </Button>
                    </div>
        }
        return (
            <div>
                <p dangerouslySetInnerHTML={html}></p>
                {buttonFail}
                {buttonSuccess}
            </div>
        )
    }
}
