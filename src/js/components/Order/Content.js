import React from "react";
import Ons from 'onsenui';
import Header from '../Header';
import User from './User';
import Checkin from './Checkin';
import Checkout from './Checkout';
import Type from './Type';
import Plate from './Plate';
import PaymentMethod from './PaymentMethod';
import Total from './Total';
import BuyButton from './BuyButton';
import SendingStatus from './SendingStatus';
import OrderSuccess from './OrderSuccess';
import AcceptNotifications from './AcceptNotifications';
import Terms from './Terms';
import Rule from './Rule';
import Alert from '../Alert';
import DialogMorning from './DialogMorning';
import Store from '../../stores/ParkingStore';
import UserStore from '../../stores/UserStore';
import MessageStore from '../../stores/MessageStore';
import PageStore from '../../stores/PageStore';
import * as Actions from '../../actions/ParkingActions';
import { Page, Button, List, ListHeader, Dialog } from 'react-onsenui';

export default class Content extends React.Component {
    constructor() {
        super();
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        let showTerms = false;

        if (this.state && typeof this.state.showTerms == 'boolean') {
            showTerms = this.state.showTerms;
        }

        return {
            sending: Store.getSending(),
            hours: Store.getHours(),
            success: Store.getSendingSuccess(),
            showTerms: showTerms,
            morning: Store.getMorning(),
            userId: UserStore.getId()
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change_hours', this.updateState);
        Store.on('change_morning', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('change_hours', this.updateState);
        Store.removeListener('change_morning', this.updateState);
    }

    renderRow(row, index) {
        switch (row) {
            case 'checkin':
                return <Checkin key={"checkin"} />;
            case 'checkout':
                return <Checkout key={"checkout"} />;
            case 'rule':
                return <Rule key={"rule"} />;
            case 'type':
                return <Type key={"type"} />;
            case 'plate':
                return <Plate key={"plate"} />;
            case 'payment':
                return <PaymentMethod key={"payment"} />;
            case 'total':
                return <Total key={"total"} />;
            case 'accept':
                return <AcceptNotifications key={"accept"} />;
            case 'terms':
                return <Terms
                        onOpen={this.handleOpenTerms.bind(this)}
                        key={"terms"} />;
        }
    }

    hideDialog() {
        Actions.setSending(false);
    }

    hideSucess() {
        Actions.setSending(false);
        Store.setSendingSuccess(false);
    }

    handleOpenTerms() {
        this.setState({
            showTerms: true
        });
    }

    handleCloseTerms() {
        this.setState({
            showTerms: false
        });
    }

    handleAbout() {
        PageStore.setName('about');
    }

    handleCloseMorning() {
        ParkingStore.setMorning(false);
    }

    render() {
        const renderRow = this.renderRow.bind(this);
        const { sending, hours, success, showTerms, morning, userId } = this.state;
        const fieldsOrder = [];
        let html;

        if (hours.amount > 0) {
            fieldsOrder.push('plate');
            fieldsOrder.push('type');
            fieldsOrder.push('rule');
            fieldsOrder.push('checkin');
            fieldsOrder.push('checkout');
        } else {
            fieldsOrder.push('type');
        }

        if (userId) {
            html = <Page renderToolbar={this.props.renderToolbar}>
                <Header />
                <List
                    dataSource={fieldsOrder}
                    renderRow={renderRow}
                    renderHeader={() => <ListHeader>Resumo da compra</ListHeader>}
                    />
                <br />
                <List
                    dataSource={["payment", "total"]}
                    renderRow={renderRow}
                    renderHeader={() => <ListHeader>detalhes do pagamento</ListHeader>}
                    />
                <br />
                <BuyButton />
                <Alert />
                <DialogMorning
                    show={morning}
                    onClose={this.handleCloseMorning.bind(this)} />
            </Page>
        } else {
            html = <Page renderToolbar={this.props.renderToolbar}>
                <User />
                <Alert />
            </Page>
        }

        return html;
    }
}
