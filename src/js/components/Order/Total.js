import React from "react";
import { ListItem, Icon } from 'react-onsenui';

import Store from '../../stores/ParkingStore';
import PaymentStore from '../../stores/PaymentStore';
import Validate from '../../utils/Validate';
import String from '../../utils/String';

export default class Total extends React.Component {
    constructor() {
        super();
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            ticket: Store.getTicket(),
            discount: PaymentStore.getDiscount(),
            method: PaymentStore.getMethod(),
        };
    }

    updateMethod() {
        this.setState(this.initialState());
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change_ticket', this.updateState);
        PaymentStore.on('change_discount', this.updateState);
        PaymentStore.on('change_method', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('change_ticket', this.updateState);
        PaymentStore.removeListener('change_discount', this.updateState);
        PaymentStore.removeListener('change_method', this.updateState);
    }

    handleClick() {
        this.props.handleClick();
    }

    render() {
        const { ticket, discount, method } = this.state;
        let price = ticket.price;

        if (method.slug === 'credit_card' && discount > 0 && ticket.amount < 10) {
            price = price-((price*discount)/100);
        }

        let value = "";

        if (ticket) {
            value = 'R$' + String.numberFormat(price, 2, ',', '.');
        }

        return (
            <ListItem>
                <div className='center'>
                    <div className="list__item__title title">Valor Total</div>
                </div>
                <div className='right'>
                    <span className="value huge">{value}</span>
                </div>
            </ListItem>
        );
    }
}
