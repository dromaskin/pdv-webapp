import React from "react";
import Ons from 'onsenui';
import { ListItem, Switch } from 'react-onsenui';
import Constants from '../../constants/AppConstants';

import Store from '../../stores/UserStore';

export default class Terms extends React.Component {
    constructor() {
        super();
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        const terms = Store.getTerms();
        return {
            needs: Store.getNeedFields(),
            terms: typeof terms == 'boolean' ? terms : false
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change_needs', this.updateState);
        Store.on('change_terms', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('change_needs', this.updateState);
        Store.removeListener('change_terms', this.updateState);
    }

    handleChange(event) {
        Store.setTerms(event.target.checked);
    }

    handleClickTerms() {
        this.props.onOpen();
    }

    handleSwitchTermsChange(event) {
        event.preventDefault();
        event.stopPropagation();
    }

    handleClickRowTerms(event) {
        event.preventDefault();
        event.stopPropagation();
        Store.setTerms(document.getElementById('switch-terms').checked);
    }

    render() {
        const { needs, terms } = this.state;

        if (needs.indexOf('terms') < 0) {
            return null;
        }

        return (
            <ListItem modifier='longdivider'>
                <div className="left title">
                    <div className="list__item__title title">
                        Li e aceito os <span className='inline-link'
                            onClick={this.handleClickTerms.bind(this)}>
                            termos de uso
                        </span>
                    </div>
                </div>
                <div className="right" onClick={this.handleClickRowTerms.bind(this)}>
                    <Switch
                        inputId='switch-terms'
                        onChange={this.handleChange.bind(this)}
                        checked={terms} />
                </div>
            </ListItem>
        );
    }
}
