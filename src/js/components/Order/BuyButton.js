import React from "react";
import { Button } from 'react-onsenui';
import Ons from 'onsenui';

import PaymentStore from '../../stores/PaymentStore';
import ParkingStore from '../../stores/ParkingStore';
import MessageStore from '../../stores/MessageStore';
import UserStore from '../../stores/UserStore';
import Validate from '../../utils/Validate';
import SendUser from '../../requests/SendUser';
import SendCreditCard from '../../requests/SendCreditCard';
import SendOrder from '../../requests/SendOrder';
import SendPayment from '../../requests/SendPayment';
import SendRefund from '../../requests/SendRefund';
import SendTicket from '../../requests/SendTicket';
import ValidateOrder from '../../requests/ValidateOrder';
import PreValidateOrder from '../../requests/PreValidateOrder';

export default class BuyButton extends React.Component {
    constructor() {
        super();
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.handleAlert = this.handleAlert.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            method: PaymentStore.getMethod(),
            data: PaymentStore.getData(),
            paymentId: PaymentStore.getId(),
            ticket: ParkingStore.getTicket(),
            userData: UserStore.getData(),
            plate: UserStore.getPlate()
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        PaymentStore.on('change', this.updateState);
        ParkingStore.on('change_ticket', this.updateState);
        UserStore.on('change', this.updateState);
        MessageStore.on('alert', this.handleAlert);
    }

    componentWillUnmount() {
        PaymentStore.removeListener('change', this.updateState);
        ParkingStore.removeListener('change_ticket', this.updateState);
        UserStore.removeListener('change', this.updateState);
        MessageStore.removeListener('alert', this.handleAlert);
    }

    sendTicket() {
        const me = this;
        SendTicket.call((response) => {
            if (!response) {
                me.sendRefund();
            }
        });
    }

    sendRefund() {
        const me = this;
        SendRefund((response) => {
            ParkingStore.setSendingFail(true);
        });
    }

    sendPayment() {
        const me = this;
        SendPayment((response) => {
            if (response) {
                me.sendTicket();
            }
        });
    }

    sendOrder() {
        const me = this;
        SendOrder((response) => {
            if (response) {
                me.sendPayment();
            }
        });
    }

    sendCreditCard() {
        const me = this;
        SendCreditCard((response) => {
            if (response) {
                me.sendOrder();
            }
        });
    }

    sendUser() {
        const needs = UserStore.getNeedFields();
        const me = this;
        SendUser((response) => {
            if (response) {
                me.sendCreditCard();
            }
        });
    }

    sendValidate() {
        const me = this;
        ValidateOrder.call((response) => {
            if (response) {
                MessageStore.set('text', 'Processando');
                MessageStore.set('title', null);
                MessageStore.setNoButton();
                if (response == 'changed') {
                    me.sendValidate();
                } else {
                    me.sendUser();
                }
            }
        });
    }

    sendPreValidate() {
        const me = this;
        PreValidateOrder.call((response) => {
            if (response) {
                MessageStore.set('text', 'Processando');
                MessageStore.set('title', null);
                MessageStore.setNoButton();
                me.sendValidate();
            }
        });
    }

    handleAlert() {
        MessageStore.hide();
    }

    handleClick() {
        MessageStore.set('text', 'Processando');
        MessageStore.setNoButton();
        MessageStore.show();
        ParkingStore.setExtensao(null);
        ParkingStore.setHoursChanged(false);
        ParkingStore.setCancelAnother(false);
        ParkingStore.setMorningTime('now');
        this.sendPreValidate();
    }

    render() {
        const { method, data, userData, plate, paymentId, ticket } = this.state;
        let button = <Button
                        className="block text-center uppercase"
                        disabled={true}>
                        Comprar Agora
                    </Button>;

        if ((Validate.paymentMethod(method, data) || paymentId) &&
            (Validate.plate(plate) || ticket.pre_charger == 1)
        ) {
            button = <Button
                        className="block text-center uppercase"
                        onClick={this.handleClick.bind(this)}>
                        Comprar Agora
                    </Button>;
        }

        return (
            <div>
                {button}
            </div>
        );
    }
}
