import React from "react";
import { ListItem } from 'react-onsenui';

import Store from '../../stores/ParkingStore';
import PageStore from '../../stores/PageStore';

export default class Rule extends React.Component {
    constructor() {
        super();
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            rule: Store.getRule()
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change_rule', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('change_rule', this.updateState);
    }

    handleClick() {
        PageStore.setName('rule');
    }

    render() {
        const { rule } = this.state;

        return (
            <ListItem
                modifier={"chevron"}
                onClick={this.handleClick.bind(this)}>
                <div className='center'>
                    <div className="list__item__title">Tipo de CAD</div>
                    <div className="list__item__subtitle">{rule.text}</div>
                </div>
                <div className='right'>
                    <span className="value">1 CAD = {rule.time} minutos</span>
                </div>
            </ListItem>
        );
    }
}
