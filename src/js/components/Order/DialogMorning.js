import React from "react";
import { AlertDialog } from 'react-onsenui';

import ParkingStore from '../../stores/ParkingStore';

export default class DialogMorning extends React.Component {
    handleSelect(time) {
        ParkingStore.setMorningTime(time);
    }

    render() {
        const buttons = [];
        const hour = (new Date()).getHours();
        buttons.push(<button
            key='now'
            onClick={this.handleSelect.bind(this, 'now')}
            className='alert-dialog-button'>
            Usar Horário Atual
        </button>);
        if (hour < 7) {
            buttons.push(<button
                key='7'
                onClick={this.handleSelect.bind(this, 7)}
                className='alert-dialog-button'>
                7:00
            </button>);
        }
        if (hour < 8) {
            buttons.push(<button
                key='8'
                onClick={this.handleSelect.bind(this, 8)}
                className='alert-dialog-button'>
                8:00
            </button>);
        }
        if (hour < 9) {
            buttons.push(<button
                key='9'
                onClick={this.handleSelect.bind(this, 9)}
                className='alert-dialog-button'>
                9:00
            </button>);
        }
        if (hour < 10) {
            buttons.push(<button
                key='10'
                onClick={this.handleSelect.bind(this, 10)}
                className='alert-dialog-button'>
                10:00
            </button>);
        }
        return (
            <AlertDialog
                isOpen={this.props.show}
                isCancelable={false}>
                <div className='alert-dialog-title'>Alerta</div>
                <div className='alert-dialog-content'>
                    Se você estiver ativando o CAD antes do horário de início de
                    funcionamento da Zona Azul no local de estacionamento
                    informe abaixo o horário de início que consta da placa
                    de sinalização.
                </div>
                <div className='alert-dialog-footer'>
                    {buttons}
                </div>
            </AlertDialog>
        );
    }
}
