import React from "react";
import { ListItem, Switch } from 'react-onsenui';

import Store from '../../stores/UserStore';

export default class AcceptNotifications extends React.Component {
    constructor() {
        super();
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            needs: Store.getNeedFields(),
            accept: typeof Store.getAcceptNotifications() == 'boolean' ? Store.getAcceptNotifications() : true
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change_needs', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('change_needs', this.updateState);
    }

    handleChange() {
        const accept = Store.getAcceptNotifications() ? Store.getAcceptNotifications() : true;
        Store.setAcceptNotifications(!accept);
    }

    render() {
        const { needs, accept } = this.state;
        let listItem = null;

        if (needs.indexOf('accept_notifications') < 0) {
            return null;
        }

        return (
            <ListItem modifier='longdivider'>
                <div className="left title">
                    <div className="list__item__title title">Aceita receber notificações?</div>
                </div>
                <div className="right">
                    <Switch
                        onChange={this.handleChange.bind(this)}
                        checked={accept} />
                </div>
            </ListItem>
        );
    }
}
