import React from 'react';
import { AlertDialog } from 'react-onsenui';

import ParkingStore from '../../stores/ParkingStore';
import PageStore from '../../stores/PageStore';

export default class OrderSuccess extends React.Component {
    constructor() {
        super();
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            hours: ParkingStore.getHours()
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        ParkingStore.on('change_hours', this.updateState);
    }

    componentWillUnmount() {
        ParkingStore.removeListener('change_hours', this.updateState);
    }

    goToReceipt() {
        this.props.onClose();
        PageStore.setName('receipt');
    }

    goToAlarm() {
        this.props.onClose();
        PageStore.setName('activeParking');
    }

    render() {
        const { hours } = this.state;
        let text;

        if (hours.amount > 0) {
            text = 'CAD' + (hours.amount > 1 ? 's' : '') + ' ativado' +
                (hours.amount > 1 ? 's' : '') + ' com sucesso!';
        } else {
            text = 'Compra de CADs com sucesso!';
        }

        return (
            <AlertDialog
                isOpen={this.props.isOpen}
                isCancelable={false}>
                <div className='alert-dialog-title'>Confirmação</div>
                <div className='alert-dialog-content'>
                    {text}
                </div>
                <div className='alert-dialog-footer alert-dialog-footer--one'>
                    <button onClick={this.goToAlarm.bind(this)} className='alert-dialog-button alert-dialog-button--one'>
                        Alarme
                    </button>
                    <button
                        onClick={this.goToReceipt.bind(this)}
                        className='alert-dialog-button alert-dialog-button--one'>
                        Ver Recibo
                    </button>
                </div>
            </AlertDialog>
        );
    }
}
