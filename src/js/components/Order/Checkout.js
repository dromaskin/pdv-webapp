import React from "react";
import { ListItem } from 'react-onsenui';

import Store from '../../stores/ParkingStore';
import DateTime from '../../utils/DateTime';
import PageStore from '../../stores/PageStore';

export default class Checkout extends React.Component {
    constructor() {
        super();
        this.state = {
            time: Store.getCheckout()
        }
        this.getTime = this.getTime.bind(this);
    }

    getTime() {
        this.setState({
            time: Store.getCheckout()
        });
    }

    componentWillMount() {
        Store.on('change', this.getTime);
    }

    componentWillUnmount() {
        Store.removeListener('change', this.getTime);
    }

    handleClick() {
        PageStore.setName('checkout');
    }

    render() {
        const { time } = this.state;
        const stringDate = DateTime.stringDate(time);
        const stringTime = DateTime.stringTime(time);
        return (
            <ListItem>
                <div className='center'>
                    <div className="list__item__title">Saída</div>
                    <div className="list__item__subtitle">{stringDate}</div>
                </div>
                <div className="right">
                    <span className="value">{stringTime}</span>
                </div>
            </ListItem>
        );
    }
}
