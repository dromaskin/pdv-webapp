import React from "react";
import { ListItem } from 'react-onsenui';

import Store from '../../stores/ParkingStore';
import PageStore from '../../stores/PageStore';

export default class Type extends React.Component {
    constructor() {
        super();
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            type: Store.getType(),
            hours: Store.getHours(),
            ticket: Store.getTicket(),
            rule: Store.getRule()
        }
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('change', this.updateState);
    }

    handleClick() {
        PageStore.setName('type');
    }

    render() {
        const { type, ticket, hours, rule } = this.state;
        let textType = type ? type.name : "Selecione o tipo de vaga";
        let description = '';
        const totalTime = rule.time * hours.amount;

        if (hours.amount == 0) {
            description = 'Não ativar';
            textType = ticket.title;
        } else if (totalTime >= 60) {
            const time = totalTime/60;
            description+= time + " Hora" + (time > 1 ? 's' : '');
            description+= " (" + hours.amount + " CAD" +
                (hours.amount > 1 ? 's' : '') + ")";
        } else {
            description+= totalTime + " Minutos";
            description+= " (" + hours.amount + " CAD" +
                (hours.amount > 1 ? 's' : '') + ")";
        }

        return (
            <ListItem modifier={"chevron"} onClick={this.handleClick.bind(this)}>
                <div className='center'>
                    <div className="list__item__title">Qtd. CADs</div>
                    <div className="list__item__subtitle">{textType}</div>
                </div>
                <div className="right">
                    <span className="value">{description}</span>
                </div>
            </ListItem>
        );
    }
}
