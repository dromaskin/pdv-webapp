import React from "react";
import { ListItem, Icon } from 'react-onsenui';

import Store from '../../stores/PaymentStore';
import UserStore from '../../stores/UserStore';
import Validate from '../../utils/Validate';
import PageStore from '../../stores/PageStore';

export default class PaymentMethod extends React.Component {
    constructor() {
        super();
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            method: Store.getMethod(),
            data: Store.getData(),
            userData: UserStore.getData(),
            id: Store.getId(),
            type: Store.getType(),
            last_digit: Store.getLastDigit(),
            name: Store.getName(),
            credits: UserStore.getCredits()
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change', this.updateState);
        UserStore.on('change_credits', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('change', this.updateState);
        UserStore.removeListener('change_credits', this.updateState);
    }

    handleClick() {
        PageStore.setName('payment');
    }

    render() {
        const { id, type, last_digit, name, method, data, userData, credits } = this.state;
        let text = "Forma de pagamento";
        let details = "";

        if (method.pre_paid == 1) {
            text = method.title;
            if (userData.guest) {
                details = <div className='right'></div>;
            } else {
                details = <div className='right'>
                    <span className="value">{credits} CAD{credits > 1 ? 's' : ''}</span>
                </div>;
            }
        } else if (id) {
            text = 'Cartão de Crédito';
            details = <div className='right'>
                <Icon icon={type} className='icon-big' />
                &nbsp;&nbsp;
                <span className="value">{last_digit}</span>
            </div>;
        } else if (Validate.paymentMethod(method, data)) {
            text = method.title;
            const number = data.number ? data.number.substr(data.number.length-4, 4) : "";
            details = <div className='right'>
                <Icon icon={method.style} className='icon-big' />
                &nbsp;&nbsp;
                <span className="value">{number}</span>
            </div>;
        } else {
            details = <div className='right'>
                <span className="link no-margin">
                    Adicionar Cartão
                </span>
            </div>;
        }

        return (
            <ListItem modifier={"chevron"} onClick={this.handleClick.bind(this)}>
                <div className='center'>
                    <div className="list__item__title title">{text}</div>
                </div>
                {details}
            </ListItem>
        );
    }
}
