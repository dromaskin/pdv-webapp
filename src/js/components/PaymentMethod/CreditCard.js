import React from 'react';

import { List, ListHeader, ListItem, Input, Icon } from 'react-onsenui';

import Validate from '../../utils/Validate';
import Card from '../../utils/Card';
import String from '../../utils/String';
import MessageStore from '../../stores/MessageStore';
import PageStore from '../../stores/PageStore';

export default class CreditCard extends React.Component {
    constructor() {
        super();
        this.hideAlert = this.hideAlert.bind(this);
        this.goHome = this.goHome.bind(this);
    }

    hideAlert() {
        MessageStore.hide();
    }

    goHome() {
        MessageStore.hide();
        PageStore.goHome();
    }

    componentDidMount() {
        this.form = document.querySelector('.payment-data');
        MessageStore.on('no', this.hideAlert);
        MessageStore.on('yes', this.goHome);
    }

    componentWillMount() {
        this.form = document.querySelector('.payment-data');
    }

    componentWillUnmount() {
        MessageStore.removeListener('no', this.hideAlert);
        MessageStore.removeListener('yes', this.goHome);
    }

    validate(callable) {
        const { fields } = this.props;
        const messages = Validate.CreditCard(fields);

        if (messages.length > 0) {
            MessageStore.setAsConfirm();
            MessageStore.set('title', 'Alerta');
            MessageStore.set('text', 'Dados insuficientes ou inválidos, deseja retornar?');
            MessageStore.set('yes', 'Sim');
            MessageStore.set('no', 'Não');
            MessageStore.show();
        } else {
            callable.call();
        }
    }

    handleNumberChange(e) {
        let value = String.numbers(e.target.value);

        if (Validate.isAmex(value)) {
            if (value.length == 15) {
                value = value.substring(0, 4) + " " + value.substring(4, 10) +
                    " " + value.substring(10, 16);
                this.form.querySelector('input[name="month"]').focus();
            } else if (value.length > 10) {
                value = value.substring(0, 4) + " " + value.substring(4, 10) +
                    " " + value.substring(10, 16);
            } else if (value.length > 4) {
                value = value.substring(0, 4) + " " + value.substring(4, 10);
            }
        } else {
            if (value.length == 16) {
                value = value.substring(0, 4) + " " + value.substring(4, 8) +
                    " " + value.substring(8, 12) + " " + value.substring(12, 16);
                this.form.querySelector('input[name="month"]').focus();
            } else if (value.length > 12) {
                value = value.substring(0, 4) + " " + value.substring(4, 8) +
                    " " + value.substring(8, 12) + " " + value.substring(12, 16);
            } else if (value.length > 8) {
                value = value.substring(0, 4) + " " + value.substring(4, 8) +
                    " " + value.substring(8, 12);
            } else if (value.length > 4) {
                value = value.substring(0, 4) + " " + value.substring(4, 8);
            }
        }

        this.props.handleChange(e, value);
    }

    handleMonthChange(e) {
        this.props.handleChange(e);
        const value = e.target.value;

        if (value.length == 2) {
            this.form.querySelector('input[name="year"]').focus();
        }
    }

    handleYearChange(e) {
        this.props.handleChange(e);
        const value = e.target.value;

        if (value.length == 4) {
            this.form.querySelector('input[name="cvv"]').focus();
        }
    }

    getNumberRow() {
        const number = this.props.fieldValue('number');
        const style = Card.creditStyle(number);
        let icon = "";

        if (style) {
            icon = <Icon icon={style} />;
        }

        return <ListItem key='credit-card-number' modifier='longdivider'>
            <div className='left'>
                {icon}
            </div>
            <div className='center'>
                <Input
                    onChange={this.handleNumberChange.bind(this)}
                    name='number'
                    pattern="[0-9]*"
                    maxlength='20'
                    placeholder='Número do cartão'
                    className='block'
                    value={number} />
            </div>
        </ListItem>
    }

    getDateRow() {
        const style = {
            width: '33%',
            display: 'inline-block'
        }
        return <ListItem key='credit-card-date' modifier='longdivider'>
            <div style={style}>
                <Input
                    onChange={this.handleMonthChange.bind(this)}
                    name='month'
                    pattern="[0-9]*"
                    maxlength='2'
                    placeholder='Mês'
                    className='block'
                    value={this.props.fieldValue('month')} />
            </div>
            <div style={style}>
                <Input
                    onChange={this.handleYearChange.bind(this)}
                    name='year'
                    pattern="[0-9]*"
                    maxlength='4'
                    placeholder='Ano'
                    className='block'
                    value={this.props.fieldValue('year').toString()} />
            </div>
            <div style={style}>
                <Input
                    onChange={this.props.handleChange}
                    name='cvv'
                    pattern="[0-9]*"
                    maxlength='4'
                    placeholder='CVV'
                    className='block'
                    value={this.props.fieldValue('cvv')} />
            </div>
        </ListItem>
    }

    renderRow(row, index) {
        switch (row) {
            case 'name':
                return this.getNameRow();
            case 'number':
                return this.getNumberRow();
            case 'date':
                return this.getDateRow();
        }
    }

    render() {
        return (
            <List
                dataSource={['number', 'date']}
                renderRow={this.renderRow.bind(this)}
                renderHeader={() => <ListHeader>Cadastrar novo cartão</ListHeader>} />
        );
    }
}
