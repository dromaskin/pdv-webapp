import dispatcher from "../dispatcher";
import Constants from "../constants/ActionsConstants";

export function setMethod(method) {
    dispatcher.dispatch({
        type: Constants.setPaymentMethod,
        method
    });
}

export function setData(data) {
    dispatcher.dispatch({
        type: Constants.setPaymentData,
        data
    });
}

export function setOptions(options) {
    dispatcher.dispatch({
        type: Constants.setPaymentOptions,
        options
    });
}
