import dispatcher from "../dispatcher";
import Constants from "../constants/ActionsConstants";

export function setEmail(email) {
    dispatcher.dispatch({
        type: Constants.setUserEmail,
        email
    });
}

export function setPassword(password) {
    dispatcher.dispatch({
        type: Constants.setUserPassword,
        password
    });
}

export function setDocument(document) {
    dispatcher.dispatch({
        type: Constants.setUserDocument,
        document
    });
}

export function setDocumentType(documentType) {
    dispatcher.dispatch({
        type: Constants.setUserDocumentType,
        documentType
    });
}

export function setBirthDate(date) {
    dispatcher.dispatch({
        type: Constants.setUserBirthDate,
        date
    });
}

export function setData(data) {
    dispatcher.dispatch({
        type: Constants.setUserData,
        data
    });
}
