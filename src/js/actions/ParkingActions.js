import dispatcher from "../dispatcher";
import Constants from "../constants/ActionsConstants";

export function setStreet(street) {
    dispatcher.dispatch({
        type: Constants.setStreet,
        street
    });
}

export function setType(data) {
    dispatcher.dispatch({
        type: Constants.setType,
        data
    });
}

export function setTypes(data) {
    dispatcher.dispatch({
        type: Constants.setTypes,
        data
    });
}

export function setCheckin(time) {
    dispatcher.dispatch({
        type: Constants.setCheckin,
        time
    });
}

export function setCheckout(time) {
    dispatcher.dispatch({
        type: Constants.setCheckout,
        time
    });
}

export function setPlate(plate) {
    dispatcher.dispatch({
        type: Constants.setPlate,
        plate
    });
}

export function setTicket(ticket) {
    dispatcher.dispatch({
        type: Constants.setTicket,
        ticket
    });
}

export function setTicketOptions(ticketOptions) {
    dispatcher.dispatch({
        type: Constants.setTicketOptions,
        ticketOptions
    });
}

export function setSending(sending) {
    dispatcher.dispatch({
        type: Constants.setSending,
        sending
    });
}

export function setPage(page) {
    dispatcher.dispatch({
        type: Constants.setPage,
        page
    });
}
