import React from "react";
import { Toolbar, BackButton, Icon } from 'react-onsenui';

import Content from '../components/Order/Content';
import Type from '../pages/Type';
import Plate from '../pages/Plate';
import Checkin from '../pages/Checkin';
import Checkout from '../pages/Checkout';
import PaymentMethod from '../pages/PaymentMethod';
import Receipt from '../pages/Receipt';
import Receipts from '../pages/Receipts';
import Rule from '../pages/Rule';
import Account from '../pages/Account';
import Login from '../pages/Login';
import PasswordRecover from '../pages/PasswordRecover';
import ActiveParking from '../pages/ActiveParking';
import Profile from '../pages/Profile';
import Scheduleds from '../pages/Scheduleds';
import Actives from '../pages/Actives';
import About from '../pages/About';
import PageStore from '../stores/PageStore';
import ParkingStore from '../stores/ParkingStore';
import UserStore from '../stores/UserStore';
import PageConstants from '../constants/PageConstants';

class PageRenderer {

    constructor() {
        this.renderToolbar = this.renderToolbar.bind(this);
        this.handleBack = this.handleBack.bind(this);
    }

    handleBack() {
        PageStore.goHome();
    }

    handleAbout() {
        PageStore.setName('about');
    }

    renderToolbar(page) {
        let title = 'Zona Azul Digital';

        if (PageConstants[page] && PageConstants[page].title) {
            title = PageConstants[page].title;
        }

        return (
            <Toolbar>
                { PageConstants[page] ?
                    <div className='left'>
                        <BackButton
                            onClick={this.handleBack.bind(this)}>
                            Voltar
                        </BackButton>
                    </div> : ''
                }
                <div className='center'>{title}</div>
                <div className='right'>
                    { PageStore.getName() != 'about' ?
                        <Icon
                            className="icon-about"
                            icon='ion-information-circled'
                            onClick={this.handleAbout.bind(this)} /> : '' }
                </div>
            </Toolbar>
        );
    }

    render(page) {
        switch (page) {
            case PageConstants.type.id:
                return <Type
                        renderToolbar={this.renderToolbar.bind(this, page)}
                        key={PageConstants.type.id} />;
            case PageConstants.checkin.id:
                return <Checkin
                            renderToolbar={this.renderToolbar.bind(this, page)}
                            minDate={new Date()}
                            key={PageConstants.checkin.id} />;
            case PageConstants.checkout.id:
                return <Checkout
                            renderToolbar={this.renderToolbar.bind(this, page)}
                            minDate={new Date()}
                            key={PageConstants.checkout.id} />;
            case PageConstants.plate.id:
                return <Plate
                            renderToolbar={this.renderToolbar.bind(this, page)}
                            key={PageConstants.plate.id} />;
            case PageConstants.payment.id:
                return <PaymentMethod
                            renderToolbar={this.renderToolbar.bind(this, page)}
                            key={PageConstants.payment.id} />;
            case PageConstants.receipt.id:
                return <Receipt
                            key={PageConstants.receipt.id} />;
            case PageConstants.receipts.id:
                return <Receipts
                            key={PageConstants.receipts.id} />;
            case PageConstants.login.id:
                return <Login
                            renderToolbar={this.renderToolbar.bind(this, page)}
                            key={PageConstants.login.id} />;
            case PageConstants.recover.id:
                return <PasswordRecover
                            renderToolbar={this.renderToolbar.bind(this, page)}
                            key={PageConstants.recover.id} />;
            case PageConstants.account.id:
                return <Account
                            renderToolbar={this.renderToolbar.bind(this, page)}
                            key={PageConstants.account.id} />;
            case PageConstants.profile.id:
                return <Profile
                            renderToolbar={this.renderToolbar.bind(this, page)}
                            key={PageConstants.profile.id} />;
            case PageConstants.activeParking.id:
                return <ActiveParking
                            renderToolbar={this.renderToolbar.bind(this, page)}
                            key={PageConstants.activeParking.id} />;
            case PageConstants.scheduleds.id:
                return <Scheduleds
                            renderToolbar={this.renderToolbar.bind(this, page)}
                            key={PageConstants.scheduleds.id} />;
            case PageConstants.actives.id:
                return <Actives
                            renderToolbar={this.renderToolbar.bind(this, page)}
                            key={PageConstants.actives.id} />;
            case PageConstants.rule.id:
                return <Rule
                            renderToolbar={this.renderToolbar.bind(this, page)}
                            key={PageConstants.rule.id} />;
            case PageConstants.about.id:
                return <About
                            renderToolbar={this.renderToolbar.bind(this, page)}
                            key={PageConstants.about.id} />;
            default:
                // return <Account
                //             renderToolbar={this.renderToolbar.bind(this, 'account')}
                //             key={PageConstants.account.id} />;
                // UserStore.setActiveParkingId(23);
                // UserStore.setActiveParking(null);
                // return <About
                //             renderToolbar={this.renderToolbar.bind(this, 'about')}
                //             key={PageConstants.about.id} />;
                return <Content
                            renderToolbar={this.renderToolbar.bind(this, false)}
                            key={"order-content"} />;
        }
    }
}

const renderer = new PageRenderer();

export default renderer;
