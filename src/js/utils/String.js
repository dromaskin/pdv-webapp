class String {
    numbers(text) {
        return text ? text.replace(/[^0-9]/g,'') : '';
    }

    ucFirst(text) {
        return text.charAt(0).toUpperCase() + text.slice(1);
    }

    strPad(input, padLength, padString, padType) {
        var half = ''
        var padToGo

        var _strPadRepeater = function(s, len) {
            var collect = ''

            while (collect.length < len) {
                collect += s
            }
            collect = collect.substr(0, len)

            return collect
        }

        input += ''
        padString = padString !== undefined ? padString : ' '

        if (padType !== 'STR_PAD_LEFT' && padType !== 'STR_PAD_RIGHT' && padType !== 'STR_PAD_BOTH') {
            padType = 'STR_PAD_LEFT'
        }
        if ((padToGo = padLength - input.length) > 0) {
            if (padType === 'STR_PAD_LEFT') {
                input = _strPadRepeater(padString, padToGo) + input
            } else if (padType === 'STR_PAD_RIGHT') {
                input = input + _strPadRepeater(padString, padToGo)
            } else if (padType === 'STR_PAD_BOTH') {
                half = _strPadRepeater(padString, Math.ceil(padToGo / 2))
                input = half + input + half
                input = input.substr(0, padLength)
            }
        }

        return input
    }

    numberFormat(number, decimals, decPoint, thousandsSep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
        var n = !isFinite(+number) ? 0 : +number
        var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
        var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
        var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
        var s = ''

        var toFixedFix = function(n, prec) {
            var k = Math.pow(10, prec)
            return '' + (Math.round(n * k) / k)
                .toFixed(prec)
        }

        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || ''
            s[1] += new Array(prec - s[1].length + 1).join('0')
        }

        return s.join(dec)
    }

    snakeCase(text) {
        return text.replace(/_\w/g, function(m) {
            return m[1].toUpperCase();
        });
    }

    removeVariableFromURL(url_string, variable_name) {
        let URL = url_string;
        let regex = new RegExp( "\\?" + variable_name + "=[^&]*&?", "gi");
        URL = URL.replace(regex,'?');
        regex = new RegExp( "\\&" + variable_name + "=[^&]*&?", "gi");
        URL = URL.replace(regex,'&');
        URL = URL.replace(/(\?|&)$/,'');
        regex = null;
        return URL;
    }

    formatDocument(numbers) {
        let document;
        if (numbers.length < 12) {
            document = numbers.substring(0, 3);

            if (numbers.length > 3) {
                document+= "." + numbers.substring(3, 6);
            }

            if (numbers.length > 6) {
                document+= "." + numbers.substring(6, 9);
            }

            if (numbers.length > 9) {
                document+= "-" + numbers.substring(9, 11);
            }
        } else {
            document = numbers.substring(0, 2);

            if (numbers.length > 2) {
                document+= "." + numbers.substring(2, 5);
            }

            if (numbers.length > 5) {
                document+= "." + numbers.substring(5, 8);
            }

            if (numbers.length > 8) {
                document+= "/" + numbers.substring(8, 12);
            }

            if (numbers.length > 12) {
                document+= "-" + numbers.substring(12, 15);
            }
        }
        return document;
    }
}

const string = new String();

export default string;
