import React from "react";
import ReactDOM from "react-dom";
import App from '../components/App';

const app = document.getElementById('app');

class AppData {
    constructor() {
        this.load = this.load.bind(this);
    }

    load() {
        ReactDOM.render(<App />, app);
    }

    reload() {
        ReactDOM.unmountComponentAtNode(app);
        this.load();
    }
}

const appData = new AppData();

export default appData;
