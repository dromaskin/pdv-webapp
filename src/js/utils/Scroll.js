export function to(element) {
    if (element.scrollIntoView) {
        element.scrollIntoView();
    }
}
