import String from './String';

class Validate {

    plate(plate) {
        return plate && (/^[A-Z]{3}-\d{4}$/.test(plate) || /^[A-Z]{3}\d{4}$/.test(plate));
    }

    email(email) {
        return email && /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/.test(email);
    }

    phone(phone) {
        return phone && /^\d{10,11}$/.test(phone);
    }

    name(name) {
        return name && name.length >= 2;
    }

    cnpj(cnpj) {
        cnpj = cnpj.replace(/[^\d]+/g,'');

        if (cnpj == '') {
            return false;
        }

        if (cnpj.length != 14)
            return false;

        if (cnpj == "00000000000000" ||
            cnpj == "11111111111111" ||
            cnpj == "22222222222222" ||
            cnpj == "33333333333333" ||
            cnpj == "44444444444444" ||
            cnpj == "55555555555555" ||
            cnpj == "66666666666666" ||
            cnpj == "77777777777777" ||
            cnpj == "88888888888888" ||
            cnpj == "99999999999999"
        ) {
            return false;
        }

        let tamanho = cnpj.length - 2;
        let numeros = cnpj.substring(0,tamanho);
        let digitos = cnpj.substring(tamanho);
        let soma = 0;
        let pos = tamanho - 7;

        for (let i = tamanho; i >= 1; i--) {
          soma += numeros.charAt(tamanho - i) * pos--;
          if (pos < 2)
                pos = 9;
        }

        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

        if (resultado != digitos.charAt(0)) {
            return false;
        }

        tamanho = tamanho + 1;
        numeros = cnpj.substring(0,tamanho);
        soma = 0;
        pos = tamanho - 7;

        for (let i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2) {
                pos = 9;
            }
        }

        let resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

        if (resultado != digitos.charAt(1)) {
            return false;
        }

        return true;
    }

    cpf(cpf) {
        let add;
        let rev;
        cpf = String.numbers(cpf);
        if(cpf == '') return false;
        if (cpf.length != 11 ||
            cpf == "00000000000" ||
            cpf == "11111111111" ||
            cpf == "22222222222" ||
            cpf == "33333333333" ||
            cpf == "44444444444" ||
            cpf == "55555555555" ||
            cpf == "66666666666" ||
            cpf == "77777777777" ||
            cpf == "88888888888" ||
            cpf == "99999999999")
                return false;
        add = 0;
        for (let i=0; i < 9; i ++)
            add += parseInt(cpf.charAt(i)) * (10 - i);
            rev = 11 - (add % 11);
            if (rev == 10 || rev == 11)
                rev = 0;
            if (rev != parseInt(cpf.charAt(9)))
                return false;
        add = 0;
        for (let i = 0; i < 10; i ++)
            add += parseInt(cpf.charAt(i)) * (11 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(10)))
            return false;
        return true;
    }

    password(password) {
        return password && password.length > 2;
    }

    document(document) {
        document = String.numbers(document);

        if (document.length == 11) {
            return this.cpf(document);
        }

        return this.cnpj(document);
    }

    user(userData, getMessages) {
        const messages = [];

        if (!this.email(userData.email)) {
            messages.push('Email inválido');
        }

        if (!this.phone(userData.phone_number)) {
            messages.push('Celular inválido');
        }

        if (!this.name(userData.name)) {
            messages.push('Nome inválido');
        }

        if (!this.name(userData.last_name)) {
            messages.push('Sobrenome inválido');
        }

        if (!this.document(userData.document)) {
            messages.push('Documento inválido');
        }

        if (!userData.id && !userData.terms) {
            messages.push('Usuário deve aceitar os termos de uso');
        }

        if (getMessages) {
            return messages;
        } else {
            return messages.length == 0;
        }
    }

    month(month) {
        return month && /^\d{1,2}$/.test(month) && month > 0 && month < 13;
    }

    year(year, minDate, maxDate) {
        if (year && year.toString().length == 2) {
            year = parseInt('20' + year);
        }

        if (year && /^\d{4}$/.test(year)) {
            if (minDate && maxDate) {
                return year >= minDate.getFullYear() && year <= maxDate.getFullYear();
            } else if (minDate) {
                return year >= minDate.getFullYear();
            } else if (maxDate) {
                return year <= maxDate.getFullYear();
            }
            return true;
        }
        return false;
    }

    isValidAmex(number) {
        return number && /^3[47][0-9]{13}$/.test(number);
    }

    isAmex(number) {
        return number && /^3[47][0-9]/.test(number);
    }

    isValidMaster(number) {
        return number && /^5[1-5][0-9]{14}$/.test(number);
    }

    isMaster(number) {
        return number && /^5[1-5][0-9]/.test(number);
    }

    isValidVisa(number) {
        return number && /^4[0-9]{12}(?:[0-9]{3})?$/.test(number);
    }

    isVisa(number) {
        return number && /^4[0-9]/.test(number);
    }

    creditCardNumber(number) {
        return this.isValidAmex(number) || this.isValidMaster(number) || this.isValidVisa(number);
    }

    creditCardCvv(cvv) {
        return cvv && /^\d{3,4}$/.test(cvv);
    }

    debitAccountNumber(number) {
        return number && /^\d{3,10}$/.test(number);
    }

    debitAccountDigit(digit) {
        return digit && /^\d{1,5}$/.test(digit);
    }

    creditCard(fields) {
        const messages = [];

        if (!this.creditCardNumber(String.numbers(fields.number))) {
            messages.push('Número do cartão inválido');
        }

        if (!this.creditCardCvv(fields.cvv)) {
            messages.push('Código do cartão inválido');
        }

        if (!this.month(fields.month)) {
            messages.push('Mês de vencimento inválido');
        }

        if (!this.year(fields.year, new Date())) {
            messages.push('Ano de vencimento inválido');
        }

        return messages;
    }

    CreditCard(fields) {
        return this.creditCard(fields);
    }

    MasterCardCredit(fields) {
        return this.creditCard(fields);
    }

    VisaCredit(fields) {
        return this.creditCard(fields);
    }

    debitAccount(fields) {
        const messages = [];

        if (!this.name(fields.name)) {
            messages.push('Nome do titular inválido');
        }

        if (!this.debitAccountNumber(fields.number)) {
            messages.push('Número da conta inválido');
        }

        if (!this.debitAccountDigit(fields.digit)) {
            messages.push('Dígito da conta inválido');
        }

        return messages;
    }

    VisaElectronDebit(fields) {
        return this.debitAccount(fields);
    }

    paymentMethod(method, data) {
        if (!(method && method.slug)) {
            return false;
        }

        switch (method.slug) {
            case 'mastercard_c':
                return this.MasterCardCredit(data).length == 0;
            case 'visa_c':
                return this.VisaCredit(data).length == 0;
            case 'visa_d':
                return this.VisaElectronDebit(data).length == 0;
            case 'credit_card':
                return this.CreditCard(data).length == 0;
            case 'pre_paid':
                return true;
            default:
                return false;
        }
    }
}

const validate = new Validate();

export default validate;
