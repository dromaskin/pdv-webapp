import Constants from '../constants/AppConstants';
import String from '../utils/String';

class DateTime {
    stringDate(date, bars) {
        const month = Constants.months[date.getMonth()];
        const day = String.strPad(date.getDate(), 2, '0');
        const year = date.getFullYear();

        if (bars) {
            return day + "/" + String.strPad((date.getMonth() + 1), 2, '0') + "/" + year;
        }

        return day + " de " + month + " de " + year;
    }

    stringTime(date) {
        const hours = String.strPad(date.getHours(), 2, '0');
        const minutes = String.strPad(date.getMinutes(), 2, '0');
        return hours + ":" + minutes;
    }

    stringDateFromMysql(string) {
        return string.split(' ')[0].split('-').reverse().join('/');
    }

    stringHourFromMysql(string, showSeconds) {
        const parts = string.split(' ')[1].split(':');

        if (!showSeconds) {
            parts.pop();
        }

        return parts.join(':');

    }

    mysqlDateFromString(date) {
        const parts = date.split('/');
        return parts.reverse().join('-');
    }

    mysqlTime(date) {
        const month = String.strPad((date.getMonth() + 1), 2, '0');
        const day = String.strPad(date.getDate(), 2, '0');
        const year = date.getFullYear();
        const hours = String.strPad(date.getHours(), 2, '0');
        const minutes = String.strPad(date.getMinutes(), 2, '0');
        const seconds = String.strPad(date.getSeconds(), 2, '0');
        return year + '-' + month + '-' + day + ' ' +
            hours + ':' + minutes + ':' + seconds;
    }

    daysDiference(min, max) {
        const minDate = min.getFullYear() + min.getMonth() + min.getDate();
        const maxDate = max.getFullYear() + max.getMonth() + max.getDate();
        return maxDate - minDate;
    }

    timeFromMiliseconds(ms) {
        const seconds = ((ms/1000) % 60) << 0;
        const minutes = ((ms/(1000*60))%60) << 0;
        const hours = ((ms/(1000*60*60))%24) << 0;
        return [hours, minutes, seconds];
    }
}

const dateTime = new DateTime();

export default dateTime;
