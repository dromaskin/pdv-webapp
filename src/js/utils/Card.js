import Validate from './Validate';

class Card {
    creditStyle(number) {
        switch (true) {
            case Validate.isAmex(number):
                return 'fa-cc-amex';
            case Validate.isMaster(number):
                return 'fa-cc-mastercard';
            case Validate.isVisa(number):
                return 'fa-cc-visa';
            default:
                return null;
        }
    }
}

const creditCard = new Card();

export default creditCard;
