import Request from '../utils/Request';

export default function ScheduleMessage(orderId, type, callback) {
    Request.post('/schedule-message', {
        order: orderId,
        type: type
    }).then((response) => {
        callback(response.data);
    });
}
