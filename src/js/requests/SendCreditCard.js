import ParkingStore from '../stores/ParkingStore';
import PaymentStore from '../stores/PaymentStore';
import MessageStore from '../stores/MessageStore';
import UserStore from '../stores/UserStore';
import Request from '../utils/Request';
import Card from '../utils/Card';
import String from '../utils/String';

export default function SendCreditCard(callback) {
    const method = PaymentStore.getMethod();

    if (method.pre_paid == 1 || PaymentStore.getId()) {
        callback(true);
    } else {
        const data = PaymentStore.getData();
        const userId = UserStore.getId();
        data.number = String.numbers(data.number);

        if (data.year && data.year.toString().length == 2) {
            data.year = parseInt('20' + data.year);
        }

        Stripe.card.createToken({
            number: data.number,
            cvc: data.cvv,
            exp_month: data.month,
            exp_year: data.year
        }, (status, response) => {
            if (status == 200 && response.id) {
                data.type = Card.creditStyle(data.number);
                data.token = response.id;
                data.user_id = userId;

                Request.post('/credit-card', {
                    credit_card: data
                }).then(function (result) {
                    const response = result && result.data ? result.data : null;
                    if (response) {
                        if (response.status && response.data && response.data.id) {
                            PaymentStore.setId(response.data.id);
                            callback(response.data.message || 'Cartão de crédito salvo');
                            return;
                        }
                    }

                    const message = response && response.message ?
                        response.message :
                        "Erro ao criar cartão de crédito";
                    MessageStore.set('text', message);
                    MessageStore.set('alert', 'Fechar');
                    MessageStore.setAsAlert();
                    callback(false);
                });
            } else {
                MessageStore.set('text', "Erro ao validar dados do cartão de crédito. Tente novamente");
                MessageStore.set('alert', 'Fechar');
                MessageStore.setAsAlert();
                callback(false);
            }
        });
    }
}
