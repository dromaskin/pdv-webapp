import UserStore from '../stores/UserStore';
import ParkingStore from '../stores/ParkingStore';
import MessageStore from '../stores/MessageStore';
import Request from '../utils/Request';
import String from '../utils/String';

export default function SendUser(callback) {
    const userData = UserStore.getData();
    userData.document = String.numbers(userData.document);
    userData.phone_number = String.numbers(userData.phone_number);
    userData.plate = UserStore.getPlate();
    userData.terms = typeof UserStore.getTerms() == 'boolean' ? UserStore.getTerms() : true;
    userData.plate = userData.plate ? userData.plate.replace(/[^0-9aA-zZ]/g,'') : '';
    const needs = UserStore.getNeedFields();

    if (needs.length > 0) {
        Request.post('/user', {
            user: userData
        }).then(function (result) {
            const response = result && result.data ? result.data : null;
            if (response) {
                if (response.status && response.data && response.data.user_id && response.data.plate_id) {
                    UserStore.setId(response.data.user_id);
                    UserStore.setPlateId(response.data.plate_id);
                    UserStore.setNeedFields([]);
                    callback(true);
                    return;
                }
            }
            const message = response && response.message ? response.message : "Erro ao criar usuário";
            MessageStore.set('text', message);
            MessageStore.set('alert', 'Fechar');
            MessageStore.setAsAlert();
            callback(false);
        });
    } else if (!UserStore.getPlateId()) {
        Request.post('/user-plate', {
            user: userData.id,
            plate: userData.plate
        }).then(function (result) {
            const response = result && result.data ? result.data : null;
            if (response) {
                if (response.status && response.data) {
                    UserStore.setPlateId(response.data);
                    callback(true);
                    return;
                }
            }
            const message = response && response.message ? response.message : "Erro ao validar placa do usuário";
            MessageStore.set('text', message);
            MessageStore.set('alert', 'Fechar');
            MessageStore.setAsAlert();
            callback(false);
        });
    } else {
        callback(true);
    }
}
