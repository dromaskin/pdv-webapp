import UserStore from '../stores/UserStore';
import ParkingStore from '../stores/ParkingStore';
import MessageStore from '../stores/MessageStore';
import PaymentStore from '../stores/PaymentStore';
import PageStore from '../stores/PageStore';

class PreValidateOrder {
    constructor() {
        this.handleYes = this.handleYes.bind(this);
        this.handleNo = this.handleNo.bind(this);
        this.handleMorningTime = this.handleMorningTime.bind(this);
        this.removeListeners = this.removeListeners.bind(this);
        this.callback = null;
    }

    handleMorningTime() {
        this.removeListeners();
        const time = ParkingStore.getMorningTime();
        ParkingStore.setHoursChanged(true);

        if (time != 'now') {
            const checkin = ParkingStore.getCheckin();
            checkin.setHours(time);
            checkin.setMinutes(0);
            ParkingStore.setCheckin(checkin);
        }

        ParkingStore.setMorning(false);
        this.callback(true);
    }

    handleYes() {
        this.removeListeners();
        this.callback(true);
    }

    handleNo() {
        this.removeListeners();
        MessageStore.hide();
        this.callback(false);
    }

    removeListeners() {
        MessageStore.removeListener('no', this.handleNo);
        MessageStore.removeListener('yes', this.handleYes);
        ParkingStore.removeListener('change_morning_time', this.handleMorningTime);
    }

    call(callback) {
        const me = this;
        const checkIn = ParkingStore.getCheckin().getHours();

        if (checkIn > 19 && checkIn <= 23) {
            MessageStore.set('text', 'Somente alguns locais da cidade tem ' +
                'funcionamento da zona azul após as 19:00.<br />Observe as ' +
                'placas de sinalização com os horários. Deseja Continuar?');
            MessageStore.set('yes', 'Sim');
            MessageStore.set('no', 'Não');
            MessageStore.setAsConfirm();
            MessageStore.on('yes', me.handleYes);
            MessageStore.on('no', me.handleNo);
            MessageStore.show();
            this.callback = callback;
        } else if (checkIn >= 0 && checkIn < 10) {
            ParkingStore.setMorning(true);
            ParkingStore.on('change_morning_time', me.handleMorningTime);
            this.callback = callback;
        } else {
            callback(true);
        }
    }
}

const request = new PreValidateOrder();

export default request;
