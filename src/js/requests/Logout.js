import Request from '../utils/Request';
import String from '../utils/String';
import Storage from '../utils/Storage';

export default function Logout() {
    Request.get('/logout').then(function (response) {
        let url = window.location.href;
        url = String.removeVariableFromURL(url, 'email');
        url = String.removeVariableFromURL(url, 'first_name');
        url = String.removeVariableFromURL(url, 'last_name');
        Storage.forget('token');
        window.location.href = url;
    });
}
