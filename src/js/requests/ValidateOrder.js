import UserStore from '../stores/UserStore';
import ParkingStore from '../stores/ParkingStore';
import MessageStore from '../stores/MessageStore';
import PaymentStore from '../stores/PaymentStore';
import PageStore from '../stores/PageStore';
import Request from '../utils/Request';
import String from '../utils/String';
import DateTime from '../utils/DateTime';

class ValidateOrder {
    constructor() {
        this.handleYes = this.handleYes.bind(this);
        this.handleNo = this.handleNo.bind(this);
        this.handleAlert = this.handleAlert.bind(this);
        this.removeListeners = this.removeListeners.bind(this);
        this.response = null;
        this.callback = null;
    }

    handleAlert() {
        this.removeListeners();
        MessageStore.hide();
        this.callback(false);
    }

    handleYes() {
        this.removeListeners();
        if (this.response.data && this.response.data.extension_id) {
            ParkingStore.setExtensao(this.response.data.extension_id);
        }

        if (this.response.data && this.response.data.cancel_another) {
            ParkingStore.setCancelAnother(true);
        }

        if (this.response.data && this.response.data.valid_from) {
            ParkingStore.setCheckin(new Date(this.response.data.valid_from));
            this.callback('changed');
        } else {
            this.callback(true);
        }
    }

    handleNo() {
        this.removeListeners();
        MessageStore.hide();
        this.callback(false);
    }

    removeListeners() {
        MessageStore.removeListener('no', this.handleNo);
        MessageStore.removeListener('yes', this.handleYes);
        MessageStore.removeListener('alert', this.handleAlert);
    }

    call(callback) {
        const me = this;
        const userId = UserStore.getId();
        const priceId = ParkingStore.getTicket().amount;
        let plate = UserStore.getPlate();
        const deviceId = UserStore.getDeviceId();
        const methodId = PaymentStore.getMethod().id;
        const spotId = ParkingStore.getType().id;
        const hours = ParkingStore.getHours().amount;
        const checkIn = ParkingStore.getCheckin();
        const checkOut = ParkingStore.getCheckout();
        const rule = ParkingStore.getRule().number;

        if (plate == '') {
            plate = 'xxx0000';
        }

        const data = {
            order: {
                amount: priceId
            },
            ticket: {
                amount: hours
            },
            parking_spot_id: spotId,
            plate: plate,
            device_id: deviceId,
            payment_method_id: methodId,
            rule: rule
        }

        if (userId) {
            data.user_id = userId;
        }

        if ((new Date().getTime()) > (checkIn.getTime()-30)) {
            data.valid_from = 'now';
        } else {
            data.valid_from = DateTime.mysqlTime(checkIn);
            data.valid_to = DateTime.mysqlTime(checkOut);
        }

        Request.post('/ticket/validate', {
            data: data
        }).then(function (result) {
            const response = result && result.data ? result.data : null;
            if (response && response.status && response.data) {
                me.response = response;
                me.callback = callback;
                MessageStore.on('yes', me.handleYes);
                MessageStore.on('no', me.handleNo);
                MessageStore.on('alert', me.handleAlert);
                MessageStore.set('yes', 'Sim');
                MessageStore.set('no', 'Não');
                MessageStore.set('title', null);
                let message;

                if (response.data.blocked) {
                    message = response.data.message;
                    MessageStore.set('alert', 'Fechar');
                    MessageStore.setAsAlert();
                } else {
                    if (response.data.allowed) {
                        const purchaseType = priceId != 10 ? 'ativação' : 'compra';
                        message = 'Você confirma a ' + purchaseType +
                            ' de ' + priceId + ' CAD' + (priceId > 1 ? 's' : '') + '?';
                    } else {
                        message = response.data.message;
                    }
                    MessageStore.setAsConfirm();
                }

                MessageStore.set('text', message);
                return;
            }


            const message = response && response.message ? response.message : "Erro ao validar a vaga";
            MessageStore.set('text', message);
            MessageStore.set('alert', 'Fechar');
            MessageStore.setAsAlert();
            callback(false);
        });
        return;
    }
}

const request = new ValidateOrder();

export default request;
