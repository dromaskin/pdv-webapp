import Request from '../utils/Request';
import UserStore from '../stores/UserStore';

export default function GetActiveParking(orderId, callback) {
    Request.post('/active', {
        order: orderId
    }).then((response) => {
        callback(response.data);
    });
}
