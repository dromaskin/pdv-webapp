import Request from '../utils/Request';

export default function CancelScheduled(orderId, callback) {
    Request.post('/cancel-scheduled', {
        order: orderId
    }).then((response) => {
        callback(response.data);
    });
}
