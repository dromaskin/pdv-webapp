import Request from '../utils/Request';
import Constants from '../constants/AppConstants';

export default function LoadTerms(callback) {
    Request.get(Constants.server_url + 'saopaulo/termos-de-uso').then((response) => {
        callback(response);
    }).catch(() => {
        callback(false);
    });
}
