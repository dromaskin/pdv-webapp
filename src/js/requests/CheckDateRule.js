import Request from '../utils/Request';
import ParkingStore from '../stores/ParkingStore';

export default function CheckDateRule(date, callback) {
    const street = ParkingStore.getStreet();
    const newDate = new Date(date.getTime());
    newDate.setHours(23);
    newDate.setMinutes(59);
    Request.post('/hours', {
        date: parseInt(newDate.getTime()/1000),
        rule: street.rule
    }).then((response) => {
        if (response && response.data) {
            const result = response.data;
            if (result.data && result.status) {
                callback(result.data);
                return;
            }
        }
        callback(false);
    }).catch((e) => {
        callback(false);
    });
}
