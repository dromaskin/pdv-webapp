import Request from '../utils/Request';

export default function RemovePlate(plateId, callback) {
    Request.post('/remove-plate', {
        plate: plateId
    }).then((response) => {
        callback(response.data);
    });
}
