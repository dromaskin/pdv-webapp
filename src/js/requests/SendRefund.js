import UserStore from '../stores/UserStore';
import ParkingStore from '../stores/ParkingStore';
import PaymentStore from '../stores/PaymentStore';
import MessageStore from '../stores/MessageStore';
import Request from '../utils/Request';

export default function SendRefund() {
    const method = PaymentStore.getMethod();

    if (method.pre_paid != 1) {
        const orderId = PaymentStore.getOrderId();

        Request.post('/refund', {
            order: orderId
        }).then(function (result) {
            const response = result && result.data ? result.data : null;
            if (response) {
                if (response.status) {
                    let message = MessageStore.get('text');
                    message+= "<br />Sua compra foi estornada.";
                    MessageStore.set('text', message);
                    MessageStore.set('alert', 'Fechar');
                    MessageStore.setAsAlert();
                    return;
                }
            }

            let message = MessageStore.get('text');
            message+= "<br />Houve um erro ao registrar estorno. Entre em contato conosco";
            MessageStore.set('text', message);
            MessageStore.set('alert', 'Fechar');
            MessageStore.setAsAlert();
        });
    } else {
        MessageStore.set('alert', 'Fechar');
        MessageStore.setAsAlert();
    }
}
