import * as ParkingActions from '../actions/ParkingActions';
import * as PaymentActions from '../actions/PaymentActions';

import UserStore from '../stores/UserStore';
import PaymentStore from '../stores/PaymentStore';
import ParkingStore from '../stores/ParkingStore';

export default function StoreData(data) {

    if (!data.street) {
        return false;
    }

    if (!data.parking_options) {
        return false;
    }

    if (!data.prices) {
        return false;
    }

    if (!data.stripe_code) {
        return false;
    }

    Stripe.setPublishableKey(data.stripe_code);

    ParkingActions.setStreet(data.street);
    ParkingActions.setTypes(data.parking_options);
    ParkingStore.setRules(data.rules);
    ParkingActions.setCheckin(new Date());
    ParkingActions.setTicketOptions(data.prices);
    PaymentActions.setOptions(data.payment_options);
    UserStore.setData(data.user_info.user);
    UserStore.setNeedFields(data.user_info.needs);

    if (data.user_info.user && data.user_info.user.plates && data.user_info.user.plates.length > 0) {
        const plate = data.user_info.user.plates[0];
        UserStore.setPlates(data.user_info.user.plates);
        UserStore.setPlate(plate.number);
        UserStore.setPlateId(plate.id);
    }

    if (data.user_info.user && data.user_info.user.cards && data.user_info.user.cards.length > 0) {
        const card = data.user_info.user.cards[0];
        UserStore.setCards(data.user_info.user.cards);
        PaymentStore.setId(card.id);
        PaymentStore.setType(card.type);
        PaymentStore.setLastDigit(card.last_digits);
        PaymentStore.setDiscount(card.discount);
        PaymentStore.setName(card.name);
    }

    if (data.user_info.user && data.user_info.user.schedules) {
        UserStore.setScheduleds(data.user_info.user.schedules);
    }

    if (data.user_info.user && data.user_info.user.actives) {
        UserStore.setActives(data.user_info.user.actives);
    }

    if (data.user_info.user && data.user_info.user.scheduleds) {
        UserStore.setScheduleds(data.user_info.user.scheduleds);
    }

    return true;
}
