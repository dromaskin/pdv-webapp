import UserStore from '../stores/UserStore';
import Request from '../utils/Request';
import String from '../utils/String';

export default function SendUpdateProfile(callback) {
    const userData = UserStore.getData();

    Request.post('/update-user', {
        user: userData
    }).then(function (result) {
        const response = result && result.data ? result.data : null;
        if (response) {
            if (response.status && response.data) {
                callback(true);
                return;
            } else {
                callback(false, response.message || 'Erro ao atualizar perfil');
            }
        }
        callback(false, '');
    });
}
