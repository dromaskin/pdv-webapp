import Request from '../utils/Request';
import Store from '../stores/UserStore';
import Storage from '../utils/Storage';
import AppData from '../utils/AppData';

export default function Login(callback) {
    const data = Store.getData();
    Request.post('/new-user', {
        user: data
    }).then(function (response) {
        if (response && response.data) {
            const result = response.data;

            if (result.status && result.data && result.data.id) {
                Request.defaults.headers = {
                    Authorization: result.data.token
                };
                Store.setNeedFields([]);
                Store.setId(result.data.id);
                Storage.set('token', result.data.token);
                AppData.reload();
            }

            callback(result);
        } else {
            callback(false);
        }
    });
}
