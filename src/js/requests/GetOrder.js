import Request from '../utils/Request';

export default function GetOrders(orderId, callback) {
    Request.get('/order/' + orderId).then((response) => {
        callback(response.data);
    });
}
