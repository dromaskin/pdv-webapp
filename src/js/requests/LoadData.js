import Request from '../utils/Request';

class Load {
    parking(callback) {
        const match = window.location.search.match(/=([aA-zZ]{3}\d{1,2}\d{2}[aA-zZ])&/);
        const meter = window.location.search.match(/meter_id=(\d{1,8})/);
        let code = match && typeof match === 'object' && match[1] ? match[1] : 'LIB0101A';
        let meterId = meter && typeof meter === 'object' && meter[1] ? meter[1] : '';
        // code = 'LIB0101A';
        Request.get('/' + code + '?meter=' + meterId).then((response) => {
            if (response && response.data) {
                callback(response.data);
            } else {
                callback({
                    status: false,
                    message: "Erro ao consultar dados da localização"
                });
            }
        });
    }
}

const load = new Load();

export default load;
