import UserStore from '../stores/UserStore';
import ParkingStore from '../stores/ParkingStore';
import PaymentStore from '../stores/PaymentStore';
import MessageStore from '../stores/MessageStore';
import Request from '../utils/Request';

export default function SendPayment(callback) {
    const method = PaymentStore.getMethod();

    if (method.pre_paid == 1) {
        callback(true);
    } else {
        const orderId = PaymentStore.getOrderId();

        Request.post('/payment', {
            payment: orderId
        }).then(function (result) {
            const response = result && result.data ? result.data : null;
            if (response) {
                if (response.status && response.data) {
                    callback(true);
                    return;
                }
            }

            const message = response && response.message ?
                response.message :
                "Erro ao registrar pagamento";
            MessageStore.set('text', message);
            MessageStore.set('alert', 'Fechar');
            MessageStore.setAsAlert();
            callback(false);
        });
    }
}
