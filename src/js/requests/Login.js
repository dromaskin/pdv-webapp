import Request from '../utils/Request';
import Store from '../stores/UserStore';
import Storage from '../utils/Storage';
import StoreUserData from '../requests/StoreUserData';

export default function Login(email, password, callback) {
    Request.post('/login', {
        email: email,
        password: password
    }).then(function (response) {
        if (response && response.data) {
            const result = response.data;

            if (result.status && result.data && result.data.user) {
                result.data.needs = [];
                StoreUserData(result.data);
                Storage.set('token', result.data.user.token);
                Request.defaults.headers = {
                    Authorization: result.data.user.token
                };
            }

            callback(result);
        } else {
            callback(false);
        }
    }).catch(function (e) {
        callback(false);
    });
}
