import UserStore from '../stores/UserStore';
import PaymentStore from '../stores/PaymentStore';
import App from '../utils/AppData';

export default function StoreUserInfo(response, callback) {

    if (response.status && response.data && response.data.user) {
        const user = response.data.user;

        if (user && user.schedules) {
            UserStore.setScheduleds(user.schedules);
        }

        if (user && user.actives) {
            UserStore.setActives(user.actives);
        }

        callback();
    } else {
        App.reload();
    }
}
