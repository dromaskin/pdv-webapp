import UserStore from '../stores/UserStore';
import ParkingStore from '../stores/ParkingStore';
import PaymentStore from '../stores/PaymentStore';
import MessageStore from '../stores/MessageStore';
import Request from '../utils/Request';
import String from '../utils/String';

export default function SendOrder(callback) {
    const userId = UserStore.getId();
    const priceId = ParkingStore.getTicket().id;
    const plateId = UserStore.getPlateId();
    const creditCardId = PaymentStore.getId();
    const deviceId = UserStore.getDeviceId();
    const methodId = PaymentStore.getMethod().id;

    const data = {
        price_id: priceId,
        user_id: userId,
        plate_id: plateId,
        user_credit_card_id: creditCardId,
        device_id: deviceId,
        payment_method_id: methodId
    }

    Request.post('/order', {
        order: data
    }).then(function (result) {
        const response = result && result.data ? result.data : null;
        if (response) {
            if (response.status && response.data) {
                PaymentStore.setOrderId(response.data);
                callback(true);
                return;
            }
        }

        const message = response && response.message ?
            response.message :
            "Erro ao criar compra";
        MessageStore.set('text', message);
        MessageStore.set('alert', 'Fechar');
        MessageStore.setAsAlert();
        MessageStore.show();
        callback(false);
    });
}
