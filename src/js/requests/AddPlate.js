import Request from '../utils/Request';
import UserStore from '../stores/UserStore';
import MessageStore from '../stores/MessageStore';

export default function AddPlate(callback) {
    const userData = UserStore.getData();
    userData.plate = userData.plate ? userData.plate.replace(/[^0-9aA-zZ]/g,'') : '';

    Request.post('/user-plate', {
        user: userData.id,
        plate: userData.plate
    }).then((response) => {
        callback(response.data);
    });
}
