import Request from '../utils/Request';

export default function GetUserInfo(callback) {
    Request.get('/user-info').then((response) => {
        callback(response.data);
    });
}
