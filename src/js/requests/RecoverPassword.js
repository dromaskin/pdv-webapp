import Request from '../utils/Request';

export default function RecoverPassword(email, callback) {
    Request.post('/recover', {
        email: email
    }).then((response) => {
        callback(response.data);
    });
}
