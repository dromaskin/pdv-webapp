import UserStore from '../stores/UserStore';
import ParkingStore from '../stores/ParkingStore';
import PaymentStore from '../stores/PaymentStore';
import MessageStore from '../stores/MessageStore';
import PageStore from '../stores/PageStore';
import Request from '../utils/Request';
import DateTime from '../utils/DateTime';

class SendTicket {

    constructor() {
        this.handleYes = this.handleYes.bind(this);
        this.handleNo = this.handleNo.bind(this);
        this.handleAlert = this.handleAlert.bind(this);
        this.removeListeners = this.removeListeners.bind(this);
    }

    handleAlert() {
        this.removeListeners();
        MessageStore.hide();
        PageStore.setName('receipt');
    }

    handleYes() {
        this.removeListeners();
        MessageStore.hide();
        PageStore.setName('receipt');
    }

    handleNo() {
        this.removeListeners();
        MessageStore.hide();
        UserStore.setActiveParking(null);
        UserStore.setActiveParkingId(PaymentStore.getOrderId());
        PageStore.setName('activeParking');
    }

    removeListeners() {
        MessageStore.removeListener('alert', this.handleAlert);
        MessageStore.removeListener('no', this.handleNo);
        MessageStore.removeListener('yes', this.handleYes);
    }

    call(callback) {
        const me = this;
        const orderId = PaymentStore.getOrderId();
        const spotId = ParkingStore.getType().id;
        const hours = ParkingStore.getHours();
        const amount = hours.amount;
        const orderAmount = ParkingStore.getTicket().amount;
        const checkIn = ParkingStore.getCheckin();
        const checkOut = ParkingStore.getCheckout();
        const street = ParkingStore.getStreet();
        const rule = ParkingStore.getRule().number;
        const extensao = ParkingStore.getExtensao();
        const hoursChanged = ParkingStore.getHoursChanged();
        const cancelAnother = ParkingStore.getCancelAnother();

        const data = {
            order_id: orderId,
            parking_spot_id: spotId,
            street_id: street.id || null,
            amount: amount,
            rule: rule,
            extension_id: extensao || null,
            lat: street.lat,
            lon: street.lon,
            hours_changed: hoursChanged,
            cancel_another: cancelAnother,
        }

        if ((new Date().getTime()) > (checkIn.getTime()-30)) {
            data.valid_from = 'now';
        } else {
            data.valid_from = DateTime.mysqlTime(checkIn);
            data.valid_to = DateTime.mysqlTime(checkOut);
        }

        Request.post('/ticket', {
            ticket: data
        }).then(function (result) {
            const response = result && result.data ? result.data : null;

            if (response) {
                if (response.status && response.data) {
                    MessageStore.on('yes', me.handleYes);
                    MessageStore.on('no', me.handleNo);
                    MessageStore.on('alert', me.handleAlert);
                    MessageStore.set('text', "CAD(s) " +
                        (orderAmount == 10 ? 'registrado(s)' : 'ativado(s)') +
                        " com sucesso");

                    if (amount > 0 && data.valid_from == 'now') {
                        MessageStore.set('yes', 'Exibir Recibo');
                        MessageStore.set('no', 'Alarme');
                        MessageStore.setAsConfirm();
                    } else {
                        MessageStore.set('alert', 'Exibir Recibo');
                        MessageStore.setAsAlert();
                    }

                    UserStore.setOrder(response.data);
                    return;
                }
            }

            const message = response && response.message ?
                response.message :
                "Erro ao ativar a vaga";
            MessageStore.set('text', typeof message == 'string' ? message : message.join('<br />'));
            MessageStore.set('alert', 'Fechar');
            callback(false);
        });

        return;
    }
}

const request = new SendTicket();

export default request;
