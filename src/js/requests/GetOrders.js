import Request from '../utils/Request';

export default function GetOrders(period, callback) {
    Request.get('/orders', {
        params: {
            period: period
        }
    }).then((response) => {
        callback(response.data);
    });
}
