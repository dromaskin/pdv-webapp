import * as ParkingActions from '../actions/ParkingActions';
import * as PaymentActions from '../actions/PaymentActions';

import UserStore from '../stores/UserStore';
import PaymentStore from '../stores/PaymentStore';
import ParkingStore from '../stores/ParkingStore';

export default function StoreUserData(data) {

    UserStore.setData(data.user);
    UserStore.setNeedFields(data.needs || []);

    if (data.user && data.user.plates && data.user.plates.length > 0) {
        const plate = data.user.plates[0];
        UserStore.setPlates(data.user.plates);
        UserStore.setPlate(plate.number);
        UserStore.setPlateId(plate.id);
    }

    if (data.user && data.user.cards && data.user.cards.length > 0) {
        const card = data.user.cards[0];
        UserStore.setCards(data.user.cards);
        PaymentStore.setId(card.id);
        PaymentStore.setType(card.type);
        PaymentStore.setName(card.name);
    }

    if (data.user && data.user.schedules) {
        UserStore.setScheduleds(data.user.schedules);
    }

    if (data.user && data.user.actives) {
        UserStore.setActives(data.user.actives);
    }

    if (data.user && data.user.scheduleds) {
        UserStore.setScheduleds(data.user.scheduleds);
    }

    return true;
}
