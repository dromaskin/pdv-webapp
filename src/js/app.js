import Ons from 'onsenui';
import Env from './utils/Env';
import AppData from './utils/AppData';

Ons.disableDeviceBackButtonHandler();
AppData.load();
