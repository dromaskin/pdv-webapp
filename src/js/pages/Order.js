import React from "react";
import Ons from 'onsenui';
import TimerMixin from 'react-timer-mixin';

import PageStore from '../stores/PageStore';
import PageConstants from '../constants/PageConstants';
import PageRenderer from '../utils/PageRenderer';
import GetUserInfo from '../requests/GetUserInfo';
import StoreUserInfo from '../requests/StoreUserInfo';
import Env from '../utils/Env';

import { Navigator } from 'react-onsenui';

export default class Order extends React.Component {
    constructor() {
        super();
        this.initialState = this.initialState.bind(this);
        this.updateState = this.updateState.bind(this);
        this.updatePage = this.updatePage.bind(this);
        this.homePage = this.homePage.bind(this);
        this.pushPage = this.pushPage.bind(this);
        this.loadUserInfo = this.loadUserInfo.bind(this);
        this.state = this.initialState();
        this.timer = null;
        this.timeCounter = 10000;
    }

    initialState() {
        return {
            page: PageStore.getName()
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    updatePage() {
        let pages = [];
        const name = PageStore.getName();

        if (name === false) {
            return;
        }

        if (this.refs.nav.pages && this.refs.nav.pages.length > 0) {
            pages = [this.refs.nav.pages[0]];
        }

        this.refs.nav.pages = pages;
        this.pushPage(this.refs.nav, name);
    }

    homePage() {
        this.handleBack(this.refs.nav);
    }

    componentDidMount() {
        const me = this;
        if (Env.isProd()) {
            me.timer = TimerMixin.setTimeout(me.loadUserInfo, me.timeCounter);
        }
    }

    loadUserInfo() {
        const me = this;
        GetUserInfo((response) => {
            StoreUserInfo(response, () => {
                if (Env.isProd()) {
                    me.timer = TimerMixin.setTimeout(me.loadUserInfo, me.timeCounter);
                }
            });
        });
    }

    componentWillMount() {
        PageStore.on('change_name', this.updatePage);
        PageStore.on('go_home', this.homePage);
    }

    componentWillUnmount() {
        PageStore.removeListener('change_name', this.updatePage);
        PageStore.removeListener('go_home', this.homePage);
        TimerMixin.clearTimeout(this.timer);
    }

    pushPage(navigator, page) {
        navigator.pushPage({
            title: PageConstants[page].title,
            hasBackButton: true,
            page: page
        });
    }

    handleBack(navigator) {
        const { form } = this.state;
        if (form && form.validate) {
            form.validate();
        }

        if (navigator.pages && navigator.pages.length > 1) {
            navigator.popPage();
        }
    }

    renderPage(route, navigator) {
        return PageRenderer.render(route.page);
    }

    render() {
        return (
            <Navigator
                renderPage={this.renderPage.bind(this)}
                ref='nav'
                initialRoute={{
                    title: 'First page',
                    hasBackButton: false
                }}
            />
        );
    }
}
