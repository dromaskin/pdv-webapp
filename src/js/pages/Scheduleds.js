import React from "react";
import { Page, BackButton, List, ListHeader, ListItem, Icon } from 'react-onsenui';

import Header from '../components/Header';
import UserStore from '../stores/UserStore';
import PageStore from '../stores/PageStore';
import MessageStore from '../stores/MessageStore';
import DateTime from '../utils/DateTime';
import GetUserInfo from '../requests/GetUserInfo';
import CancelScheduled from '../requests/CancelScheduled';
import StoreUserInfo from '../requests/StoreUserInfo';

export default class Scheduleds extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.handleYes = this.handleYes.bind(this);
        this.handleNo = this.handleNo.bind(this);
        this.handleAlert = this.handleAlert.bind(this);
        this.state = this.initialState();
        this.parkingCancel = null;
    }

    initialState() {
        return {
            orders: UserStore.getScheduleds()
        }
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        UserStore.on('change_scheduleds', this.updateState);
        MessageStore.on('yes', this.handleYes);
        MessageStore.on('no', this.handleNo);
        MessageStore.on('alert', this.handleAlert);
    }

    componentWillUnmount() {
        UserStore.removeListener('change_scheduleds', this.updateState);
        MessageStore.removeListener('yes', this.handleYes);
        MessageStore.removeListener('no', this.handleNo);
        MessageStore.removeListener('alert', this.handleAlert);
    }

    handleAlert() {
        GetUserInfo((response) => {
            StoreUserInfo(response, () => {
                MessageStore.hide();
            });
        });
    }

    handleYes() {
        const me = this;
        MessageStore.set('title', 'Cancelamento');
        MessageStore.set('text', 'Enviando');
        MessageStore.setNoButton();
        CancelScheduled(me.parkingCancel.order_id, (response) => {
            if (response.status) {
                me.parkingCancel = null;
                GetUserInfo((response) => {
                    StoreUserInfo(response, () => {
                        MessageStore.set('title', 'Cancelamento');
                        MessageStore.set('text', 'Vaga cancelada');
                        setTimeout(() => {
                            MessageStore.hide();
                        }, 1000);
                    });
                });
            } else {
                MessageStore.set('text', response.message || 'Erro ao cancelar vaga');
                MessageStore.set('alert', 'Fechar');
                MessageStore.setAsAlert();
            }
        });
    }

    handleNo() {
        MessageStore.hide();
    }

    handleClickRemove(row) {
        this.parkingCancel = row;
        MessageStore.set('title', 'Cancelamento');
        MessageStore.set('text', 'Confirma o cancelamento da vaga?');
        MessageStore.set('yes', 'Sim');
        MessageStore.set('no', 'Não');
        MessageStore.setAsConfirm();
        MessageStore.show();
    }

    renderRow(row, index) {
        if (row == 'error') {
            return (
                <ListItem key={index} modifier='longdivider'>
                    <div className='center'>
                        <div className="list__item__title title">
                            Nenhuma vaga reservada
                        </div>
                    </div>
                </ListItem>
            );
        }
        return (
            <ListItem key={index} modifier='longdivider'>
                <div className='left'>
                    <div className="list__item__title">{row.amount} CAD{row.amount > 1 ? 's' : ''}</div>
                </div>
                <div className='center'>
                    <div className="list__item__title title">
                        De {DateTime.stringDateFromMysql(row.valid_from)}&nbsp;
                        {DateTime.stringHourFromMysql(row.valid_from)}&nbsp;
                        Até {DateTime.stringDateFromMysql(row.valid_to)}&nbsp;
                        {DateTime.stringHourFromMysql(row.valid_to)}
                    </div>
                </div>
                <div className="right">
                    <Icon
                        icon='ion-ios-close'
                        onClick={this.handleClickRemove.bind(this, row)}
                        className='icon-big text-danger' />
                </div>
            </ListItem>
        );
    }

    render() {
        const { orders } = this.state;

        let fields;
        if (orders && orders.length > 0) {
            fields = orders;
        } else {
            fields = ['error'];
        }

        return (
            <Page renderToolbar={this.props.renderToolbar}>
                <Header />
                <List
                    dataSource={fields}
                    renderRow={this.renderRow.bind(this)}
                    renderHeader={() => <ListHeader>vagas agendadas</ListHeader>} />
            </Page>
        );
    }
}
