import React from 'react';
import { Page, Button, Input, List, ListHeader, ListItem } from 'react-onsenui';
import Ons from 'onsenui';

import Validate from '../utils/Validate';
import Header from '../components/Header';
import RecoverPassword from '../requests/RecoverPassword';
import PageStore from '../stores/PageStore';
import MessageStore from '../stores/MessageStore';
import AppData from '../utils/AppData';

export default class PassworRecover extends React.Component {
    constructor() {
        super();
        this.state = {
            email: null,
            sending: false
        }
        this.handleYes = this.handleYes.bind(this);
        this.handleNo = this.handleNo.bind(this);
        this.handleAlert = this.handleAlert.bind(this);
    }

    componentWillMount() {
        MessageStore.on('yes', this.handleYes);
        MessageStore.on('no', this.handleNo);
    }

    componentWillUnmount() {
        MessageStore.removeListener('yes', this.handleYes);
        MessageStore.removeListener('no', this.handleNo);
    }

    handleYes() {
        const { email } = this.state;

        MessageStore.set('text', 'Enviando email de recuperação de senha');
        MessageStore.setNoButton();

        RecoverPassword(email, function (response) {
            if (response && response.status) {
                MessageStore.set('text', 'Email de recuperação de senha ' +
                    'enviado<br />Verifique sua caixa de entrada');
            } else {
                MessageStore.set('text', response.message ||
                    'Erro ao enviar email de recuperação de senha');
            }

            MessageStore.set('alert', 'Fechar');
            MessageStore.setAsAlert();
        });
    }

    handleNo() {
        MessageStore.hide();
    }

    handleAlert() {
        MessageStore.hide();
    }

    handleChange(e) {
        const state = this.state;
        state[e.target.getAttribute('name')] = e.target.value;
        this.setState(state);
    }

    handleClick() {
        MessageStore.set('text', 'Confirma a recuperação de senha?');
        MessageStore.set('yes', 'Sim');
        MessageStore.set('no', 'Não');
        MessageStore.setAsConfirm();
        MessageStore.show();
    }

    renderRow(row, index) {
        switch (row) {
            case 'email':
                return <ListItem key='email' modifier='longdivider'>
                    <Input
                        onChange={this.handleChange.bind(this)}
                        type='email'
                        name='email'
                        placeholder='Email'
                        className='block'
                        value={this.state.email} />
                </ListItem>;
        }
    }

    componentDidMount() {
        setTimeout(() => {
            const form = document.querySelector('#form-login');
            const input = form.querySelector('input[name=email]');
            input.focus();
        }, 200);
    }

    render() {
        const { email, sending } = this.state;
        const valid = Validate.email(email);
        const disabled = !valid;

        return(
            <Page
                id="form-login"
                renderToolbar={this.props.renderToolbar}>
                <List
                    dataSource={['email']}
                    renderRow={this.renderRow.bind(this)}
                    renderHeader={() => <ListHeader>email de acesso</ListHeader>}
                    />
                <br />
                <Button
                    className="block text-center uppercase"
                    disabled={disabled}
                    onClick={this.handleClick.bind(this)}>
                    Enviar
                </Button>
            </Page>
        );
    }
}
