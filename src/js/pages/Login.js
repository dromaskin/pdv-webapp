import React from 'react';
import { Page, Button, Input, Toolbar, List, ListHeader, ListItem } from 'react-onsenui';
import Ons from 'onsenui';

import Validate from '../utils/Validate';
import Header from '../components/Header';
import SendLogin from '../requests/Login';
import PageStore from '../stores/PageStore';
import AppData from '../utils/AppData';

export default class Login extends React.Component {
    constructor() {
        super();
        this.state = {
            email: null,
            password: null,
            sending: false
        }
    }

    handleChange(e) {
        const state = this.state;
        state[e.target.getAttribute('name')] = e.target.value;
        this.setState(state);
    }

    handleClick() {
        const { email, password } = this.state;
        const me = this;

        this.setState({
            sending: true
        });

        SendLogin(email, password, function (response) {
            if (response) {
                if (response.status) {
                    AppData.reload();
                    return;
                } else {
                    Ons.notification.alert(response.message || 'Erro ao consultar dados do usuário', {
                        title: "Erro"
                    });
                }
            } else {
                Ons.notification.alert('Erro ao consultar dados do usuário', {
                    title: "Erro"
                });
            }
            me.setState({
                sending: false
            });
        });
    }

    handleRecover() {
        PageStore.setName('recover');
    }

    renderRow(row, index) {
        switch (row) {
            case 'email':
                let value = this.state.email || '';
                return <ListItem key='email' modifier='longdivider'>
                    <Input
                        onChange={this.handleChange.bind(this)}
                        name='email'
                        placeholder='Email / Usuário'
                        className='block'
                        value={value} />
                </ListItem>;
            case 'password':
                return <ListItem key='password' modifier='longdivider'>
                    <Input
                        type='password'
                        onChange={this.handleChange.bind(this)}
                        name='password'
                        placeholder='Senha'
                        className='block'
                        value={this.state.password || ''} />
                </ListItem>;
        }
    }

    componentDidMount() {
        setTimeout(() => {
            const form = document.querySelector('#form-login');
            const input = form.querySelector('input[name=email]');
            input.focus();
        }, 200);
    }

    render() {
        const { email, password, sending } = this.state;
        const valid = Validate.password(password);
        const disabled = !valid || sending;
        const text = sending ? "Enviando" : "Enviar";

        return(
            <Page
                id="form-login"
                renderToolbar={this.props.renderToolbar}>
                <List
                    dataSource={['email', 'password']}
                    renderRow={this.renderRow.bind(this)}
                    renderHeader={() => <ListHeader>Dados de acesso</ListHeader>}
                    />
                <br />
                <Button
                    className="block text-center uppercase"
                    disabled={disabled}
                    onClick={this.handleClick.bind(this)}>
                    Enviar
                </Button>
                <br />
                <span
                    className="link login text-center"
                    onClick={this.handleRecover.bind(this)}>
                    Esqueceu sua senha? Clique aqui.
                </span>
            </Page>
        );
    }
}
