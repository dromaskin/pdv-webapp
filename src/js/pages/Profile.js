import React from "react";
import Ons from 'onsenui';
import { Page, Button, List, ListHeader, ListItem } from 'react-onsenui';

import Store from '../stores/UserStore';
import PageStore from '../stores/PageStore';
import SendUpdateProfile from '../requests/SendUpdateProfile';
import Header from '../components/Header';
import Email from '../components/Order/User/Email';
import Document from '../components/Order/User/Document';
import Name from '../components/Order/User/Name';
import LastName from '../components/Order/User/LastName';
import PhoneNumber from '../components/Order/User/PhoneNumber';
import Password from '../components/Order/User/Password';
import Validate from '../utils/Validate';

export default class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.initialState = this.initialState.bind(this);
        this.updateState = this.updateState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            changePassword: this.state && this.state.changePassword ? this.state.changePassword : false,
            sending: this.state && this.state.sending ? this.state.sending : false,
            data: Store.getData()
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('change', this.updateState);
    }

    handleShowPassword() {
        const changePassword = !this.state.changePassword;

        if (!changePassword) {
            Store.setPassword('');
        }

        this.setState({changePassword});
    }

    handleSend() {
        const me = this;
        me.setState({
            sending: true
        });
        SendUpdateProfile((status, message) => {
            if (status) {
                Ons.notification.alert('Perfil atualizado com sucesso', {
                    title: "Sucesso"
                }).then(() => {
                    me.setState({
                        sending: false
                    });
                    PageStore.setName('account');
                });
            } else {
                Ons.notification.alert(message, {
                    title: "Erro"
                }).then(() => {
                    me.setState({
                        sending: false
                    });
                });
            }
        });
    }

    renderRow(row, index) {
        switch (row) {
            case 'email':
                return <Email key='user-email' />;
            case 'document':
                return <Document key='user-document' />;
            case 'name':
                return <Name key='name' />;
            case 'last_name':
                return <LastName key='last-name' />;
            case 'phone_number':
                return <PhoneNumber key='phone-number' />;
            case 'password':
                return <Password key='password' />;
            default:
                return '';
        }
    }

    render() {
        const { changePassword, data, sending } = this.state;
        let valid = Validate.user(data);
        let messages = Validate.user(data, true);
        let fields = ['name', 'last_name', 'email', 'document', 'phone_number'];
        let passwordText = 'Alterar Senha';

        if (changePassword) {
            fields.push('password');
            passwordText = 'Ocultar Senha';
        }

        if (changePassword && data.password.length < 3) {
            valid = false;
        }

        return (
            <Page renderToolbar={this.props.renderToolbar}>
                <Header />
                <List
                    dataSource={fields}
                    renderRow={this.renderRow.bind(this)}
                    renderHeader={() => <ListHeader>Meus dados</ListHeader>} />
                <span
                    className="link login text-center"
                    onClick={this.handleShowPassword.bind(this)}>
                    {passwordText}
                </span>
                <Button disabled={!valid || sending} className="block text-center uppercase"
                    onClick={this.handleSend.bind(this)}>
                    {sending ? 'enviando' : 'enviar'}
                </Button>
            </Page>
        );
    }
}
