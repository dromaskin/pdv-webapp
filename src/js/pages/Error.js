import React from "react";
import { Page, Toolbar } from 'react-onsenui';

export default class Loading extends React.Component {
    renderToolbar(route, navigator) {
        return (
            <Toolbar>
                <div className='center'>Zona Azul Digital</div>
            </Toolbar>
        );
    }
    render() {
        const { message } = this.props;
        return(
            <Page renderToolbar={this.renderToolbar.bind(this)}>
                <div className="text-center">
                    {message}
                </div>
            </Page>
        );
    }
}
