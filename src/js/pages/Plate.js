import React from "react";
import { Page, Button, List, ListHeader, ListItem, Input, Icon } from 'react-onsenui';
import Header from "../components/Header";
import Store from '../stores/UserStore';
import PageStore from '../stores/PageStore';
import MessageStore from '../stores/MessageStore';
import AddPlate from '../requests/AddPlate';
import RemovePlate from '../requests/RemovePlate';
import * as Actions from '../actions/ParkingActions';
import Validate from '../utils/Validate';

export default class Plate extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.handleYes = this.handleYes.bind(this);
        this.handleNo = this.handleNo.bind(this);
        this.state = this.initialState();
        this.removePlate = null;
    }

    initialState() {
        return {
            data: Store.getData(),
            plate: Store.getPlate(),
            plate_id: Store.getPlateId(),
            plates: Store.getPlates(),
            id: Store.getId()
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change_plate', this.updateState);
        Store.on('change_plate_id', this.updateState);
        Store.on('change_plates', this.updateState);
        MessageStore.on('yes', this.handleYes);
        MessageStore.on('no', this.handleNo);
    }

    componentWillUnmount() {
        Store.removeListener('change_plate', this.updateState);
        Store.removeListener('change_plate_id', this.updateState);
        Store.removeListener('change_plates', this.updateState);
        MessageStore.removeListener('yes', this.handleYes);
        MessageStore.removeListener('no', this.handleNo);
    }

    componentDidMount() {
        setTimeout(() => {
            const form = document.querySelector('#form-plate');
            const input = form.querySelector('input[name=plate]');
            input.focus();
        }, 200);
    }

    handleYes() {
        const { plates } = this.state;
        const me = this;
        MessageStore.set('text', 'Removendo placa');
        MessageStore.setNoButton();
        RemovePlate(this.removePlate.id, (response) => {
            if (response && response.status) {
                for (let i in plates) {
                    if (plates[i].id == me.removePlate.id) {
                        plates.splice(i, 1);
                    }
                }
                Store.setPlates(plates);
                MessageStore.hide();
            } else {
                MessageStore.set('text', response.message || 'Erro ao remover placa');
                MessageStore.set('alert', 'Fechar');
                MessageStore.setAsAlert();
            }
            me.removePlate = null;
        });
    }

    handleNo() {
        this.removePlate = null;
        MessageStore.hide();
    }

    handleChange(e) {
        let plate = e.target.value;

        this.setState({
            plate: plate.toUpperCase()
        });
    }

    handleClick() {
        const { id, plates } = this.state;
        Actions.setPlate(this.state.plate);

        if (id) {
            MessageStore.set('text', 'Enviando');
            MessageStore.setNoButton();
            MessageStore.show();
            AddPlate((response) => {
                if (response.status && response.data) {
                    plates.push({
                        id: response.data,
                        user_id: id,
                        number: this.state.plate
                    });
                    Store.setPlateId(response.data);
                    Store.setPlates(plates);
                    MessageStore.set('text', 'Placa salva');
                    setTimeout(() => {
                        MessageStore.hide();
                        PageStore.goHome();
                    }, 1000);
                } else {
                    MessageStore.set('text', response.message || 'Erro ao salvar placa');
                    setTimeout(() => {
                        MessageStore.hide();
                    }, 1000);
                }
            });
        } else {
            PageStore.goHome();
        }
    }

    handleClickRemove(plate) {
        this.removePlate = plate;
        MessageStore.set('text', 'Deseja remover a placa ' + plate.number + '?');
        MessageStore.set('yes', 'Sim');
        MessageStore.set('no', 'Não');
        MessageStore.setAsConfirm();
        MessageStore.show();
    }

    handleClickPlate(plate) {
        const { plate_id } = this.state;

        if (plate.id != plate_id) {
            Store.setPlate(plate.number);
            Store.setPlateId(plate.id);
            PageStore.goHome();
        }
    }

    renderRowPlates(row, index) {
        let icon = '';
        const { plate_id, plate, data } = this.state;

        if ((plate_id && row.id == plate_id) || plate == row.number) {
            icon = <Icon icon='ion-ios-checkmark' className='icon-big text-success' />;
        } else if (!data.guest) {
            icon = <Icon
                icon='ion-ios-close'
                onClick={this.handleClickRemove.bind(this, row)}
                className='icon-big text-danger' />;
        }

        return (
            <ListItem key={index} modifier='longdivider'>
                <div className='center' onClick={this.handleClickPlate.bind(this, row)}>
                    <div className="list__item__title">{row.number}</div>
                </div>
                <div className="right">
                    {icon}
                </div>
            </ListItem>
        );
    }

    renderRowInput(row, index) {
        const { plate, plates, id } = this.state;
        let plateNumber = plate;

        if (id) {
            for (let i in plates) {
                if (plates[i].number == plate) {
                    plateNumber = '';
                }
            }
        }

        return (
            <ListItem key={'input-plate'} modifier='longdivider'>
                <Input
                    style={{width:'100%'}}
                    value={plateNumber}
                    maxlength='8'
                    name='plate'
                    onKeyUp={this.handleChange.bind(this)}
                    placeholder='Número da placa' />
            </ListItem>
        );
    }

    render() {
        const { plate, plates, id, data } = this.state;
        let text = 'Confirmar';
        let valid = true;

        if (id) {
            text = 'adicionar nova placa';
            for (let i in plates) {
                if (plates[i].number == plate) {
                    valid = false;
                }
            }
        }

        let list = '';
        let button = <Button
                        className="block text-center uppercase"
                        onClick={this.handleClick.bind(this)}>
                        {text}
                    </Button>;

        if (!Validate.plate(plate) || !valid) {
            button = <Button
                        className="block text-center uppercase"
                        disabled={true}>
                        {text}
                    </Button>;
        }

        if (plates && plates.length > 0) {
            list = <List
                dataSource={plates}
                renderRow={this.renderRowPlates.bind(this)}
                renderHeader={() => <ListHeader>Placas cadastradas</ListHeader>}
                />
        }

        let form = '';

        if (!data.guest) {
            form = <div>
                <List
                    dataSource={['input']}
                    renderRow={this.renderRowInput.bind(this)}
                    renderHeader={() => <ListHeader>Cadastrar</ListHeader>}
                    />
                <br />
                {button}
                <br />
            </div>;
        }

        return (
            <Page id="form-plate" renderToolbar={this.props.renderToolbar}>
                {form}
                {list}
            </Page>
        );
    }
}
