import React from "react";
import { Page, Range, Fab, Icon, Dialog, List, ListHeader, ListItem, Button } from 'react-onsenui';

import Header from "../components/Header";
import Store from '../stores/ParkingStore';
import CheckDateRule from '../requests/CheckDateRule';
import PageStore from '../stores/PageStore';
import MessageStore from '../stores/MessageStore';
import * as Actions from '../actions/ParkingActions';
import DateTime from '../utils/DateTime';
import String from '../utils/String';
import Calendar from '../components/DateTime/Calendar';
import LoadingRules from '../components/DateTime/LoadingRules';

export default class Checkin extends React.Component {
    constructor(props) {
        super(props);
        this.initialState = this.initialState.bind(this);
        this.updateState = this.updateState.bind(this);
        this.handleCloseLoading = this.handleCloseLoading.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            time: Store.getCheckin(),
            tempTime: new Date(Store.getCheckin().getTime()),
            minHours: 6,
            maxHours: 23,
            minMinutes: 0,
            maxMinutes: 59,
            minDate: this.props.minDate,
            editing: false
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        MessageStore.on('alert', this.handleCloseLoading);
    }

    componentWillUnmount() {
        MessageStore.removeListener('alert', this.handleCloseLoading);
        Actions.setCheckin(this.state.time);
    }

    handleCloseLoading() {
        MessageStore.hide();
    }
    // update hours with the range value
    handleUpdateHour(e) {
        const { time } = this.state;
        time.setHours(parseInt(e.target.value));
        this.setState({
            time
        });
    }
    // update minutes with the range value
    handleUpdateMinutes(e) {
        const { time } = this.state;
        time.setMinutes(parseInt(e.target.value));
        this.setState({
            time
        });
    }

    handleEdit() {
        this.setState({
            editing: true
        });
    }

    handleEdited() {
        this.setState({
            editing: false
        });
    }

    handleDatePickerChange(date) {
        const me = this;
        this.setState({
            editing: false,
            tempTime: date
        });
        MessageStore.setNoButton();
        MessageStore.set('text', 'Carregando');
        MessageStore.show();
        CheckDateRule(date, (response) => {
            if (response.allowed) {
                me.handleCloseLoading(response, date);
            } else {
                MessageStore.set('title', null, true);
                MessageStore.set('alert', 'OK', true);
                MessageStore.set('text', response.message || 'Erro ao consultar horário da vaga');
                MessageStore.setAsAlert();
            }
        });
    }

    getBoundaries(min, max) {
        const hours = Math.floor(Store.getHours().time / 60);
        let minHours = parseInt(min), maxHours = parseInt(max);

        maxHours-= hours;

        return [minHours, maxHours];
    }

    setBoundaries(min, max) {
        const hours = Math.floor(Store.getHours().time / 60);
        let minHours, maxHours;

        minHours = parseInt(min);
        maxHours = parseInt(max)-hours;

        this.setState({minHours,maxHours});
    }

    handleCloseLoading(response, date) {
        MessageStore.hide();
        if (response && response.allowed) {
            const [minHours, maxHours] = this.getBoundaries(
                response.rule.from,
                response.rule.to
            );
            this.setState({
                minHours,
                maxHours
            });
            this.resetTime(date, minHours, 0);
        } else {
            const tempTime = new Date(this.state.time.getTime());
            this.setState({
                tempTime
            });
        }
    }
    // reset the minutes and hours to minimum
    resetTime(date, hours, minutes) {
        const inputs = document.querySelectorAll('input[type=range]');
        const newDate = new Date(date.getTime());
        newDate.setHours(hours);
        newDate.setMinutes(minutes);
        this.setState({
            time: newDate
        });

        for (let i in inputs) {
            if (inputs[i].value) {
                if (inputs[i].getAttribute('name') == 'hours') {
                    inputs[i].setAttribute('value', hours);
                } else {
                    inputs[i].setAttribute('value', 0);
                }
            }
        }

        const ranges = document.querySelectorAll('.range__left');

        for (let i in ranges) {
            if (ranges[i].style) {
                ranges[i].setAttribute('style', 'width: 0%');
            }
        }
    }

    handleBack() {
        PageStore.goHome();
    }

    renderRow(row, index) {
        const {
            time,
            minHours,
            maxHours,
            minMinutes,
            maxMinutes
        } = this.state;
        switch (row) {
            case 'date':
                const dateString = DateTime.stringDate(time, true);
                return (
                    <ListItem modifier='chevron' key={index} onClick={this.handleEdit.bind(this)}>
                        <div className='left'>
                            <div className="list__item__title">Data</div>
                        </div>
                        <div className="right">
                            <span className="value">
                                {dateString}
                                &nbsp;
                                <Icon icon='fa-calendar' />
                            </span>
                        </div>
                    </ListItem>
                );
            case 'hour':
                const hours = {
                    value: time.getHours(),
                    min: minHours,
                    max: maxHours
                }
                return (
                    <ListItem key={index}>
                        <div className='left'>
                            <div className="list__item__title">
                                {String.strPad(hours.value, 2, '0')} Hora(s)
                            </div>
                        </div>
                        <div className="center">
                            <Range
                                key={"hours"}
                                onChange={this.handleUpdateHour.bind(this)}
                                min={hours.min}
                                max={hours.max}
                                name='hours'
                                value={hours.value}
                                style={{width: "98%"}} />
                        </div>
                    </ListItem>
                );
            case 'minute':
                const minutes = {
                    value: time.getMinutes(),
                    min: minMinutes,
                    max: maxMinutes
                }
                return (
                    <ListItem key={index}>
                        <div className='left'>
                            <div className="list__item__title">
                                {String.strPad(minutes.value, 2, '0')} Minuto(s)
                            </div>
                        </div>
                        <div className="center">
                            <Range
                                key={"minutes"}
                                onChange={this.handleUpdateMinutes.bind(this)}
                                min={minutes.min}
                                max={minutes.max}
                                name='minutes'
                                value={minutes.value}
                                style={{width: "98%"}} />
                        </div>
                    </ListItem>
                );
            default:
                return '';
        }
    }

    render() {
        const {
            tempTime,
            time,
            editing
        } = this.state;

        return(
            <Page renderToolbar={this.props.renderToolbar}>
                <Header />
                <List
                    dataSource={['date', 'hour', 'minute']}
                    renderRow={this.renderRow.bind(this)}
                    renderHeader={() => <ListHeader>Entrada</ListHeader>} />
                <br />
                <Button
                    className="block text-center uppercase"
                    onClick={this.handleBack.bind(this)}>
                    Confirmar
                </Button>
                <Dialog
                    isOpen={editing}
                    isCancelable={true}
                    onCancel={this.handleEdited.bind(this)}>
                    <Calendar
                        date={time}
                        handleChange={this.handleDatePickerChange.bind(this)} />
                </Dialog>
            </Page>
        );
    }
}
