import React from "react";
import { Page, List, ListHeader, ListItem, Icon, Button } from 'react-onsenui';

import UserStore from '../stores/UserStore';
import MessageStore from '../stores/MessageStore';
import PageStore from '../stores/PageStore';
import Header from "../components/Header";
import DateTime from "../utils/DateTime";
import String from "../utils/String";
import GetActiveParking from "../requests/GetActiveParking";
import ScheduleMessage from "../requests/ScheduleMessage";

export default class ActiveParking extends React.Component {
    constructor() {
        super();
        this.initialState = this.initialState.bind(this);
        this.updateState = this.updateState.bind(this);
        this.drawClock = this.drawClock.bind(this);
        this.prepareClock = this.prepareClock.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            active: UserStore.getActiveParking(),
            id: UserStore.getActiveParkingId(),
            size: null,
            percent: 0,
            marginVertical: 0,
            marginHorizontal: 0
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        UserStore.on('change_active_parking', this.updateState);
        UserStore.on('change_active_parking_id', this.updateState);
        MessageStore.on('alert', this.handleAlert);
    }

    componentWillUnmount() {
        UserStore.removeListener('change_active_parking', this.updateState);
        UserStore.removeListener('change_active_parking_id', this.updateState);
        MessageStore.removeListener('alert', this.handleAlert);
    }

    handleAlert() {
        MessageStore.hide();
    }

    componentDidMount() {
        const [size, marginVertical, marginHorizontal] = this.clockSize();
        this.setState({size, marginVertical, marginHorizontal});
        this.validateParking();
    }

    validateParking() {
        const me = this;
        MessageStore.set('text', 'Validando dados do estacionamento');
        MessageStore.setNoButton();
        MessageStore.show();
        const { id } = this.state;
        GetActiveParking(id, (response) => {
            if (response && response.status && response.data) {
                UserStore.setActiveParking(response.data);
                MessageStore.hide();
                me.prepareClock();
            } else {
                MessageStore.set('text', response.message || 'Erro ao buscar dados da vaga');
                MessageStore.set('alert', 'Fechar');
                MessageStore.setAsAlert();
            }
        });
    }

    getLeftTime(active) {
        const now = new Date();
        const validTo = new Date(active.valid_to);
        return validTo.getTime() - now.getTime();
    }

    clockPercent() {
        const { active } = this.state;
        if (active) {
            const from = parseInt(active.valid_from);
            const to = parseInt(active.valid_to);
            const left = (new Date()).getTime()-from;
            const total = to-from;
            return (left*100)/total;
        } else {
            return 0;
        }
    }

    clockSize() {
        const navigation = document.querySelector('.navigation-bar').offsetHeight;
        const header = document.querySelector('.header').offsetHeight;
        const width = window.innerWidth ||
            document.documentElement.clientWidth ||
            document.body.clientWidth;
        const screen = window.innerHeight ||
            document.documentElement.clientHeight ||
            document.body.clientHeight;
        const height = screen-(navigation+header);

        const size = height < width ? height*.6 : width*.6;
        const marginVertical = (height-size)/2;
        const marginHorizontal = (width-size)/2;

        return [size, marginVertical, marginHorizontal];
    }

    prepareClock() {
        const [size, marginVertical, marginHorizontal] = this.clockSize();
        this.setState({size, marginVertical, marginHorizontal});
        const line = 15;

        if (!size) {
            return;
        }

        this.canvas = document.querySelector('#parking-countdown');
        this.context = this.canvas.getContext('2d');
        this.radius = (size - line) / 2;
        this.canvas.width = this.canvas.height = size;
        document.querySelector('.countdown-text').setAttribute('style',
            'margin-top: ' + (size*0.4) + 'px');
        this.context.translate(size / 2, size / 2);
        this.context.rotate((-1 / 2 + 0 / 180) * Math.PI);
		this.context.beginPath();
		this.context.arc(0, 0, this.radius, 0, Math.PI * 2 * 1, false);
		this.context.strokeStyle = '#efefef';
        this.context.lineCap = 'round';
		this.context.lineWidth = line;
		this.context.stroke();
        this.drawClock();
    }

    drawClock() {
        if (!this.radius || !document.querySelector('.countdown-text')) {
            return;
        }

        const percent = this.clockPercent();
        this.setState({percent});
        const { size } = this.state;
        const line = 15;
        const left = this.getLeftTime(this.state.active);
        let color = '#555555';

        if (window.getComputedStyle) {
            const style = getComputedStyle(document.querySelector('.countdown-text'));
            color = style.color;
        }

        const perc = Math.min(Math.max(0, (percent/100) || 1), 1);
		this.context.beginPath();
		this.context.arc(0, 0, this.radius, 0, Math.PI * 2 * perc, false);
		this.context.strokeStyle = color;
        this.context.lineCap = 'round';
		this.context.lineWidth = line;
		this.context.stroke();

        if (percent < 100) {
            setTimeout(() => {
                this.drawClock()
            }, 1000);
        }
    }

    handleButtonMessageClick(type) {
        const orderId = this.state.active.order_id;
        if (type == 'end') {
            MessageStore.set('text', 'Enviando');
            MessageStore.set('title', null);
            MessageStore.setNoButton();
            MessageStore.show();
            ScheduleMessage(orderId, type, (response) => {
                if (response.status) {
                    MessageStore.set('text', 'Alarme ativado para o fim do período');
                } else {
                    MessageStore.set('text', response.message ||
                        'Erro ao ativar alarme');
                }
                MessageStore.set('alert', 'Fechar');
                MessageStore.setAsAlert();
            });
        } else {
            MessageStore.set('text', 'Enviando');
            MessageStore.set('title', null);
            MessageStore.setNoButton();
            MessageStore.show();
            ScheduleMessage(orderId, type, (response) => {
                ScheduleMessage(orderId, 'almost', (response) => {
                    if (response.status) {
                        MessageStore.set('text', 'Alarme ativado para o fim do período');
                    } else {
                        MessageStore.set('text', response.message ||
                            'Erro ao ativar alarme');
                    }
                    MessageStore.set('alert', 'Fechar');
                    MessageStore.setAsAlert();
                });
            });
        }
    }

    render() {
        const { percent, active } = this.state;

        if (active) {
            const [hours, minutes, seconds] = DateTime.timeFromMiliseconds(this.getLeftTime(this.state.active));
            const stringHours = String.strPad(hours, 2, '0');
            const stringMinutes = String.strPad(minutes, 2, '0');
            const stringSeconds = String.strPad(seconds, 2, '0');

            return (
                <Page renderToolbar={this.props.renderToolbar}>
                    <Header />
                    <br />
                    <div className="text-center">
                        <Button
                            className="uppercase details"
                            onClick={this.handleButtonMessageClick.bind(this, 'end')}>
                            acionar alarme via sms
                            <span className="button-small">ao final do período</span>
                        </Button>
                        <br />
                        <br />
                        <Button
                            className="uppercase details"
                            onClick={this.handleButtonMessageClick.bind(this, 'both')}>
                            acionar alarme via sms
                            <span className="button-small">10 minutos antes do fim e</span>
                            <span className="button-small">no final do período</span>
                        </Button>
                    </div>
                    <div className="countdown-wrapper">
                        <canvas className="countdown-canvas" id='parking-countdown'></canvas>
                        <div className="countdown-text">
                            {stringHours}h{stringMinutes}m
                            <span className="countdown-plate">{active.plate}</span>
                        </div>
                    </div>
                </Page>
            );
        } else {
            return (
                <Page renderToolbar={this.props.renderToolbar}>
                    <Header />
                    <div className="countdown-wrapper">
                        <canvas className="countdown-canvas" id='parking-countdown'></canvas>
                        <div className="countdown-text">
                            <span className="countdown-plate"></span>
                        </div>
                    </div>
                </Page>
            )
        }
    }
}
