import React from "react";
import { Page, Button, Input, List, ListItem, ListHeader, Icon, Toolbar, BackButton } from 'react-onsenui';
import Header from "../components/Header";
import Store from '../stores/PaymentStore';
import UserStore from '../stores/UserStore';
import MessageStore from '../stores/MessageStore';
import PageStore from '../stores/PageStore';
import ParkingStore from '../stores/ParkingStore';
import CreditCard from '../components/PaymentMethod/CreditCard';
import * as Actions from '../actions/PaymentActions';
import * as Scroll from '../utils/Scroll';
import Validate from '../utils/Validate';
import SendCreditCard from '../requests/SendCreditCard';

export default class PaymentMethod extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.handleClickCard = this.handleClickCard.bind(this);
        this.handleAlert = this.handleAlert.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            id: Store.getId(),
            method: Store.getMethod(),
            data: Store.getData(),
            userData: UserStore.getData(),
            options: Store.getOptions(),
            userId: UserStore.getId(),
            cards: UserStore.getCards(),
            credits: UserStore.getCredits(),
            success: false
        };
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change', this.updateState);
        UserStore.on('change_cards', this.updateState);
        MessageStore.on('alert', this.handleAlert);
    }

    componentWillUnmount() {
        Store.removeListener('change', this.updateState);
        UserStore.removeListener('change_cards', this.updateState);
        MessageStore.removeListener('alert', this.handleAlert);
    }

    handleAlert() {
        const { success } = this.state;
        MessageStore.hide();

        if (success) {
            PageStore.goHome();
        }
    }

    getFieldValue(field) {
        const { data } = this.state;
        return data[field] ? data[field] : '';
    }

    handleFormChange(e, value) {
        const { data } = this.state;
        data[e.target.name] = !value ? e.target.value : value;
        Actions.setData(data);
        Store.setId(null);
    }

    handleMethodClick(item) {
        const { method, cards } = this.state;
        if (method.id != item.id) {
            Actions.setMethod(item);
            Actions.setData({});
            Store.setId(null);

            if (item.pre_paid == 1) {
                ParkingStore.setTicket(ParkingStore.getTicketOptions()[0]);
            } else if (cards.length > 0) {
                this.updateCard(cards[0]);
            }

            Scroll.to(document.querySelector('.payment-data'));
        }
    }

    getPaymentForm(method) {
        const { userData } = this.state;

        if (!userData.guest && method) {
            switch (method.slug) {
                case 'credit_card':
                    return <CreditCard
                                ref={method.slug}
                                handleChange={this.handleFormChange.bind(this)}
                                fieldValue={this.getFieldValue.bind(this)}
                                fields={this.state.data} />;
                default:
                    return "";
            }
        }

        return '';
    }

    handleBack() {
        const { id, userId, method, cards, data } = this.state;
        const me = this;
        if ((method && this.refs[method.slug]) && !id) {
            this.refs[method.slug].validate(() => {
                if (userId) {
                    MessageStore.set('text', 'Enviando');
                    MessageStore.setNoButton();
                    MessageStore.show();
                    SendCreditCard((response) => {
                        if (typeof response === 'string') {
                            data.last_digits = data.number.substr(-4, 4);
                            cards.push(data);
                            UserStore.setCards(cards);
                            Store.setType(data.type);
                            Store.setLastDigit(data.last_digits);
                            Store.setName(data.name);
                            Store.setData({});
                            me.setState({
                                success: true
                            });
                            MessageStore.set('text', response);
                            MessageStore.set('alert', 'OK');
                            MessageStore.setAsAlert();
                            MessageStore.show();
                        } else {
                            MessageStore.set('text', 'Erro ao adicionar cartão');
                            MessageStore.set('alert', 'OK');
                            MessageStore.setAsAlert();
                            MessageStore.show();
                        }
                    });
                } else {

                }
            });
        } else {
            PageStore.goHome();
        }
    }

    updateCard(card) {
        Store.setId(card.id);
        Store.setType(card.type);
        Store.setLastDigit(card.last_digits);
        Store.setDiscount(card.discount);
        Store.setName(card.name);
    }

    handleClickCard(card) {
        const { id } = this.state;

        if (card.id != id) {
            this.updateCard(card);
            PageStore.goHome();
        }
    }

    renderToolbar() {
        const backButton = <BackButton onClick={this.handleBack.bind(this)}>Voltar</BackButton>;
        return (
            <Toolbar>
                <div className='left'>{backButton}</div>
                <div className='center'>Forma de Pagamento</div>
            </Toolbar>
        );
    }

    renderRowCard(row, index) {
        let icon = '';
        const { id } = this.state;

        if (id && row.id == id) {
            icon = <Icon icon='ion-ios-checkmark' className='icon-big text-success' />;
        }

        let creditCardName = '';

        if (/mastercard/.test(row.type)) {
            creditCardName = "MasterCard";
        } else if (/visa/.test(row.type)) {
            creditCardName = "Visa";
        } else if (/amex/.test(row.type)) {
            creditCardName = "American Express";
        }

        return (
            <ListItem key={index} modifier='longdivider' onClick={this.handleClickCard.bind(this, row)}>
                <div className='left'>
                    <Icon icon={row.type} />
                </div>
                <div className='center'>
                    <div className="list__item__title title">{creditCardName}</div>
                </div>
                <div className="right">
                    {row.last_digits}
                    &nbsp;&nbsp;
                    {icon}
                </div>
            </ListItem>
        );
    }

    renderRow(row, index) {
        let icon = '';
        const { method, credits, userData } = this.state;

        if (method && row.id == method.id) {
            icon = <Icon icon='ion-ios-checkmark' className='icon-big text-success' />;
        }

        let details = <div className="right">
            <Icon icon={row.style} className='icon-big' />
            &nbsp;&nbsp;
            {icon}
        </div>;

        if (row.pre_paid == 1) {
            if (userData.guest) {
                details = <div className="right">{icon}</div>;
            } else {
                details = <div className="right">
                    {credits} CAD{credits > 1 ? 's' : ''}
                    &nbsp;&nbsp;
                    {icon}
                </div>;
            }
        }

        return (
            <ListItem key={index} modifier='longdivider' onClick={this.handleMethodClick.bind(this, row)}>
                <div className='center'>
                    <div className="list__item__title">{row.title}</div>
                </div>
                {details}
            </ListItem>
        );
    }

    render() {
        const { options, method, cards } = this.state;
        const form = this.getPaymentForm(method);
        let list = '';

        if (cards && cards.length > 0 && method.slug == 'credit_card') {
            list = <List
                        dataSource={cards}
                        renderRow={this.renderRowCard.bind(this)}
                        renderHeader={() => <ListHeader>Cartões cadastrados</ListHeader>} />;
        }

        return (
            <Page renderToolbar={this.renderToolbar.bind(this)}>
                <List
                    dataSource={options}
                    renderRow={this.renderRow.bind(this)}
                    renderHeader={() => <ListHeader>Formas de pagamento</ListHeader>}
                    />
                {list}
                <div className='payment-data form'>
                    {form}
                </div>
                <br />
                <Button
                    className="block text-center uppercase"
                    onClick={this.handleBack.bind(this)}>
                    Confirmar
                </Button>
            </Page>
        );
    }
}
