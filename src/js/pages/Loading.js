import React from "react";
import { Page, Toolbar, ProgressCircular } from 'react-onsenui';

export default class Loading extends React.Component {
    renderToolbar(route, navigator) {
        return (
            <Toolbar>
                <div className='center'>Zona Azul Digital</div>
            </Toolbar>
        );
    }
    render() {
        return(
            <Page renderToolbar={this.renderToolbar.bind(this)}>
                <div className="text-center">
                    <br />
                    <br />
                    <ProgressCircular indeterminate />
                    <br />
                    <br />
                    Carregando Dados
                </div>
            </Page>
        );
    }
}
