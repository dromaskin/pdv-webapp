import React from "react";
import { Page, Toolbar, BackButton, ProgressCircular,List, ListHeader, ListItem } from 'react-onsenui';

import Header from '../components/Header';
import GetOrders from '../requests/GetOrders';
import UserStore from '../stores/UserStore';
import PageStore from '../stores/PageStore';
import DateTime from '../utils/DateTime';

export default class Receipts extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        const orders = UserStore.getOrders();
        const loading = orders.length < 1;
        return {
            orders,
            loading
        }
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        UserStore.on('change_orders', this.updateState);
    }

    componentWillUnmount() {
        UserStore.removeListener('change_orders', this.updateState);
    }

    componentDidMount() {
        GetOrders((response) => {
            let orders = [];

            if (response && response.status && response.data) {
                orders = response.data;
            }

            UserStore.setOrders(orders);
        });
    }

    handleBack() {
        PageStore.goHome();
    }

    handleClick(row) {
        UserStore.setOrder(row.id);
        PageStore.setName('receipt');
    }

    renderRow(row, index) {
        let detail = '';

        if (row.date) {
            detail = <span className="value">
                {DateTime.stringDateFromMysql(row.date)}&nbsp;
                {DateTime.stringHourFromMysql(row.date)}
            </span>;
        }

        return (
            <ListItem key={index} modifier="chevron" onClick={this.handleClick.bind(this, row)}>
                <div className='center'>
                    <div className="list__item__title">{row.amount} CAD{row.amount > 1 ? 's' : ''}</div>
                </div>
                <div className="right">
                    {detail}
                </div>
            </ListItem>
        );
    }

    renderToolbar() {
        return (
            <Toolbar>
                <div className='left'>
                    <BackButton onClick={this.handleBack.bind(this)}>Voltar</BackButton>
                </div>
                <div className='center'>Minhas Compras</div>
            </Toolbar>
        );
    }

    render() {
        const { orders, loading } = this.state;
        let list = '';

        if (loading) {
            list = <div className="text-center">
                        <br />
                        <ProgressCircular indeterminate={true} />
                        <br />
                        Carregando
                    </div>;
        } else {
            if (orders && orders.length > 0) {
                list = <List
                    dataSource={orders}
                    renderRow={this.renderRow.bind(this)}
                    renderHeader={() => <ListHeader>Compras</ListHeader>} />
            } else {
                list = <div className="text-center">
                        <br />
                        <h4>Nenhuma compra encontrada</h4>
                    </div>
            }
        }

        return (
            <Page renderToolbar={this.renderToolbar.bind(this)}>
                <Header />
                {list}
            </Page>
        );
    }
}
