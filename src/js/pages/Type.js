import React from "react";
import { Page, Button, List, ListHeader, ListItem, Icon } from 'react-onsenui';
import Header from "../components/Header";
import * as Actions from '../actions/ParkingActions';
import Store from '../stores/ParkingStore';
import PageStore from '../stores/PageStore';
import PaymentStore from '../stores/PaymentStore';
import String from '../utils/String';

export default class Type extends React.Component {

    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            type: Store.getType(),
            types: Store.getTypes(),
            ticket: Store.getTicket(),
            rule: Store.getRule(),
            ticketOptions: Store.getTicketOptions(),
            hours: Store.getHours(),
            method: PaymentStore.getMethod(),
        }
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        Store.on('change', this.updateState);
        PaymentStore.on('change_method', this.updateState);
    }

    componentWillUnmount() {
        Store.removeListener('change', this.updateState);
        PaymentStore.removeListener('change_method', this.updateState);
    }

    handleClickType(item) {
        Actions.setType(item);
    }

    handleClickTicket(item) {
        Actions.setTicket(item);
    }

    handleClickHour(item) {
        Store.setHours(item);
    }

    handleBack() {
        PageStore.goHome();
    }

    renderRowHours(row, index) {
        let icon = '';
        let description = '';
        const { hours, rule } = this.state;
        const totalTime = rule.time * row.amount;


        if (row.pre_charger == 1) {
            row = {
                id: 0,
                amount: 0,
                title: 'Não ativar'
            }
        }

        if (hours && row.id == hours.id) {
            icon = <Icon icon='ion-ios-checkmark' className='icon-big text-success' />;
        }

        if (totalTime >= 60) {
            const time = totalTime/60;
            description+= time + " Hora" + (time > 1 ? 's' : '');
        } else {
            description+= totalTime + " Minutos";
        }

        if (row.amount == 0) {
            description = row.title;
        }

        return (
            <ListItem key={index} onClick={this.handleClickHour.bind(this, row)}>
                <div className='center'>
                    <div className="list__item__title">{row.amount} CAD{row.amount > 1 ? 's' : ''}</div>
                </div>
                <div className="right">
                    <span className="value">{description}</span>
                    &nbsp;&nbsp;
                    {icon}
                </div>
            </ListItem>
        );
    }

    renderRowTicket(row, index) {
        let icon = '';
        let description = '';
        const { ticket, hours, method, rule } = this.state;
        const totalTime = rule.time * row.amount;

        if (row.pre_charger == 1 && method.pre_paid == 1) {
            return '';
        }

        if (row.id == ticket.id) {
            icon = <Icon icon='ion-ios-checkmark' className='icon-big text-success' />;
        }

        if (totalTime >= 60) {
            const time = totalTime/60;
            description+= time + " Hora" + (time > 1 ? 's' : '');
        } else {
            description+= totalTime + " Minutos";
        }

        description+= " (" + row.amount + " CAD" + (row.amount > 1 ? 's' : '') + ")";

        if (row.pre_charger == 1) {
            description = row.title;
        }

        return (
            <ListItem key={index} onClick={this.handleClickTicket.bind(this, row)}>
                <div className='center'>
                    <div className="list__item__title">R${String.numberFormat(row.price, 2, ',', '.')}</div>
                </div>
                <div className="right">
                    <span className="value">{description}</span>
                    &nbsp;&nbsp;
                    {icon}
                </div>
            </ListItem>
        );
    }

    renderRowType(row, index) {
        let icon = '';
        const { type, rule } = this.state;

        // hiding types when selected rule is 1
        if (rule.number == 1 && row.slug != 'caminhao') {
            return '';
        }

        if (row.id == type.id) {
            icon = <div className="right"><Icon icon='ion-ios-checkmark' className='icon-big text-success' /></div>;
        }

        return (
            <ListItem key={index} onClick={this.handleClickType.bind(this, row)}>
                <div className="center">
                    <div className="list__item__title title">{row.name}</div>
                </div>
                {icon}
            </ListItem>
        );
    }

    render() {
        const { ticket, types, ticketOptions, hours } = this.state;
        let listHours = '';
        let listTypes = '';

        if (ticket.pre_charger == 1) {
            listHours = <div>
                            <br />
                            <List
                                dataSource={ticketOptions}
                                renderRow={this.renderRowHours.bind(this)}
                                renderHeader={() => <ListHeader>Ativação</ListHeader>} />
                        </div>;
        }

        if (hours.amount > 0) {
            listTypes = <div>
                            <List
                                dataSource={types}
                                renderRow={this.renderRowType.bind(this)}
                                renderHeader={() => <ListHeader>Tipos de usuário</ListHeader>} />
                            <br />
                        </div>;
        }

        return (
            <Page renderToolbar={this.props.renderToolbar}>
                <List
                    dataSource={ticketOptions}
                    renderRow={this.renderRowTicket.bind(this)}
                    renderHeader={() => <ListHeader>Permanência</ListHeader>} />
                {listTypes}
                <br />
                <Button
                    className="block text-center uppercase"
                    onClick={this.handleBack.bind(this)}>
                    Confirmar
                </Button>
            </Page>
        );
    }
}
