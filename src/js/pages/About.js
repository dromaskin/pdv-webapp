import React from 'react';
import { Page } from 'react-onsenui';

import Header from '../components/Header';

export default class About extends React.Component {
    render() {
        return(
            <Page
                id="form-login"
                renderToolbar={this.props.renderToolbar}>
                <div className="about">
                    <h1>INFORMAÇÕES SOBRE A ZONA AZUL DIGITAL EM SÃO PAULO</h1>
                    O Estacionamento Rotativo Zona Azul no município de
                    São Paulo é regulamentado pelos Decretos nº 11.661 de
                    30/12/74 e nº 17.115 de 05/01/81.
                    <br />
                    <br />
                    O <b>Cartão Azul Digital – CAD</b> deve ser utilizado para
                    regularizar o estacionamento do veículo, quando a placa de
                    sinalização apresenta a mensagem "OBRIGATÓRIO CARTÃO AZUL".
                    <br />
                    <br />
                    <img src="/images/cartao_obrigatorio.png" />
                    <br />
                    <br />
                    É permitido usar 1 ou no máximo 2 Cartões/CAD para
                    estacionar na mesma vaga durante o tempo de validade do
                    Cartão/CAD. O veículo deve ser retirado da vaga após o
                    término do tempo permitido, caso contrário será considerado
                    estacionado de forma irregular e ficará sujeito a aplicação
                    de penalidade (multa grave – 5 pontos) e medida
                    administrativa (guinchamento).
                    <br />
                    <br />
                    Observe sempre a placa de sinalização para verificar o tempo
                    de validade do Cartão/CAD, os dias da semana, o horário de
                    funcionamento e demais condições para estacionamento.
                    <br />
                    <br />
                    O <b>TEMPO DE VALIDADE DO CARTÃO/CAD</b> varia de acordo com o
                    local de estacionamento:
                    <br />
                    <br />
                    <b>REGRA GERAL</b>: vale para a maioria das vagas – use 1
                    Cartão/CAD para 1 hora e 2 Cartões/CAD para 2 horas.
                    Esta regra vale quando a placa de sinalização não apresenta
                    o tempo de validade do Cartão/CAD.
                    <br />
                    <br />
                    <img src="/images/regra_geral.png" />
                    <br />
                    <br />
                    <b>BOLSÃO CAMINHÃO</b>: nos trechos de via sinalizados para
                    o estacionamento de caminhão, caminhonete e veículo misto –
                    use <b>1 Cartão/CAD para 30 minutos e 2 Cartões/CAD para 1
                    hora</b> - conforme indicado na placa. Atenção: quando
                    estes tipos de veículos usam as vagas fora do Bolsão
                    Caminhão ou estacionam no Bolsão Caminhão da Rua Santa
                    Rosa, vale a REGRA GERAL (1 Cartão/CAD para 1 hora e 2
                    Cartões/CAD para 2 horas).
                    <br />
                    <br />
                    <img src="/images/bolsao_caminhao.png" />
                    <br />
                    <br />
                    <b>ÁREAS ESPECIAIS</b>: em algumas áreas consideradas
                    especiais <b>o tempo de validade do Cartão é diferente e está
                    indicado na placa de sinalização</b>. <i>Exemplos</i>: no Parque
                    Ibirapuera e no Parque da Aclimação, 1 Cartão/CAD para 2
                    horas e 2 Cartões/CAD para 4 horas; na Praça Charles Miller
                    1 Cartão/CAD para 3 horas e 2 Cartões/CAD para 6 horas.
                    <br />
                    <br />
                    <img src="/images/areas_especiais.png" />
                    <br />
                    <br />
                    Quando o Cartão/CAD for ativado antes do horário de início
                    da Zona Azul, é necessário indicar no Aplicativo o horário
                    de início que consta da placa de sinalização.
                    <br />
                    <br />
                    Não é necessário deixar nenhum comprovante de pagamento do
                    CAD no interior do veículo.
                    <br />
                    <br />
                    Nas vagas reservadas para pessoas idosas, portadoras de
                    deficiência ou com dificuldade de locomoção, também é
                    obrigatório portar no veículo o Cartão emitido pelo
                    Departamento de Operação do Sistema Viário – DSV.
                    <br />
                    <br />
                    À Prefeitura do Município de São Paulo e à Companhia de
                    Engenharia de Tráfego – CET não caberão, em nenhuma
                    hipótese, responsabilidade indenizatória por acidentes,
                    danos, furtos ou prejuízos que os veículos ou seus
                    proprietários possam vir a sofrer nos locais delimitados
                    pelo Estacionamento Rotativo Zona Azul.
                    <br />
                    <br />
                    Para outras informações consulte o site da CET/SP e das
                    empresas distribuidoras de Cartão/CAD.
                    <br />
                    <br />
                    <h3>Sem conexão à internet</h3>
                    Para ativar um CAD em São Paulo via SMS, observe a
                    sinalização da rua e envie uma mensagem para 29000 com o texto:
                    <b>ATIVARCAD,{"{CPF}"},{"{Placa}"},{"{Qtd}"},{"{Regra}"}</b>,
                    sem espaços entre as vírgulas.
                    <br />
                    <br />
                    Quantidade: 1 ou 2
                    <br />
                    <br />
                    Regra 1: 1 CAD = 30 minutos
                    <br />
                    Regra 2: 1 CAD = 60 minutos
                    <br />
                    Regra 3: 1 CAD = 90 minutos
                    <br />
                    Regra 4: 1 CAD = 120 minutos
                </div>
            </Page>
        );
    }
}
