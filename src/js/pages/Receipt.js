import React from "react";
import { Page, Icon } from 'react-onsenui';

import AppData from '../utils/AppData';
import Header from '../components/Header';
import UserStore from '../stores/UserStore';
import GetOrder from '../requests/GetOrder';
import DateTime from '../utils/DateTime';
import String from '../utils/String';

export default class Receipt extends React.Component {
    constructor() {
        super();
        this.state = {
            orderId: UserStore.getOrder(),
            order: {},
            clicked: false,
            loading: true,
            user: UserStore.getData()
        }
    }

    componentDidMount() {
        const state = {
            loading: false
        };
        const me = this;
        const { order, orderId } = this.state;
        GetOrder(orderId, (response) => {
            if (response.status && response.data) {
                state.order = response.data;
            }
            me.setState(state);
        });
    }

    handleBack() {
        AppData.reload();
    }

    render() {
        const { orders, loading, order, user } = this.state;
        let html = '';
        let isDiscount = false;
        if (loading) {
            html = <h3 className="loading">Carregando dados da compra</h3>;
        } else {
            order.emaillink = "mailto:"+order.email;
            isDiscount = order.is_discount;
            if (order.date) {
                if (order.time >= 60) {
                    let time = (Math.floor(order.time/60));
                    order.time = time + " hora" + (time > 1 ? 's' : '');
                } else {
                    order.time = order.time + ' minutos';
                }

                order.time+= order.amount > 1 ? " cada" : "";
            }

            html = <div className="body">
                    { order.date ?
                        <div>
                            <div className="user">
                                {user.name} {user.last_name}
                                <div className="plate">{order.plate}</div>
                            </div>
                        </div> :
                        <div>
                            <div className="user">
                                {user.name} {user.last_name}
                                <div className="plate">{String.formatDocument(user.document)}</div>
                            </div>
                        </div> }
                    <div className="auth">
                        <div className="message">Compra realizada</div>
                        <div className="details">Autenticação {order.auth}</div>
                        <div className="bill">
                            <span className="currency">R$</span>
                            <span className="value">
                                {String.numberFormat(order.price, 2, ',', '.')}
                            </span>
                            <div className="type">
                                {
                                    order.date ?
                                    <span className="title">
                                        {order.amount} cad{order.amount > 1 ? 's' : ''} de {order.time}
                                    </span> :
                                    <span className="title">
                                        {order.amount} cad{order.amount > 1 ? 's' : ''}
                                    </span>
                                }
                                <span className="details">{order.method}</span>
                            </div>
                            {
                                order.date ?
                                <div className="parking">
                                    <div className="boxes">
                                        <div className="details">
                                            Data/Hora {order.amount == 10 ? 'Compra' : 'Ativação'}
                                        </div>
                                        <div className="box">
                                            <span className="date">
                                                {DateTime.stringDateFromMysql(order.from)}
                                            </span>
                                            <br />
                                            <span className="hour">
                                                {DateTime.stringHourFromMysql(order.from)}
                                            </span>
                                        </div>
                                    </div>
                                </div> :
                                ""
                            }
                            { order.cancel_another == 1 ?
                                <span className="details">
                                    <br />
                                    * Esta ativação cancelou CADs que estavam vigentes
                                </span> : '' }
                            { order.extension_id ?
                                <span className="details">
                                    <br />
                                    * Continuação do CAD Anterior
                                </span> : '' }
                            { order.hours_changed == 1 ?
                                <span className="details">
                                    <br />
                                    * Hora do início da Zona Azul&nbsp;
                                    {DateTime.stringHourFromMysql(order.from)}
                                </span> : '' }
                            <div className="nav">
                                <span onClick={this.handleBack.bind(this)}>
                                    <Icon icon="ion-home" />
                                    Voltar
                                </span>
                            </div>
                            <div className="info">
                                <span className="title">Sugestões e reclamações</span>
                                <br />
                                <a href={order.site}>{order.site}</a>
                                <br />
                                <a href={order.emaillink}>{order.email}</a>
                                <br />
                                Ligue: {order.short_phone}
                            </div>
                        </div>
                    </div>
                </div>
        }

        return (
            <Page className={"receipt" + (isDiscount ? " discount" : "")}>
                <div className="hd">
                    <div className={"parkme" + (isDiscount ? " min" : "")}></div>
                    { isDiscount ? <div className="bradesco"></div> : null }
                </div>
                <div className="body">
                    {html}
                </div>
            </Page>
        );
    }
}
