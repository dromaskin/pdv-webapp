import React from "react";
import { Page, Toolbar, BackButton } from 'react-onsenui';

export default class PurchaseError extends React.Component {
    handleBack() {
        this.props.handleBack.call();
    }
    renderToolbar() {
        return (
            <Toolbar>
                <div className='left'>
                    <BackButton onClick={this.handleBack.bind(this)}>Voltar</BackButton>
                </div>
                <div className='center'>Zona Azul Digital</div>
            </Toolbar>
        );
    }
    render() {
        return (
            <Page renderToolbar={this.renderToolbar.bind(this)}>
                <h1>Erro ao tentar efetuar a compra</h1>
            </Page>
        );
    }
}
