import React from "react";
import { Page, Button, List, ListHeader, ListItem, Icon } from 'react-onsenui';

import Header from '../components/Header';
import ParkingStore from '../stores/ParkingStore';
import PageStore from '../stores/PageStore';

export default class Scheduleds extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            rules: ParkingStore.getRules(),
            rule: ParkingStore.getRule()
        }
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        ParkingStore.on('change_rule', this.updateState);
        ParkingStore.on('change_rules', this.updateState);
    }

    componentWillUnmount() {
        ParkingStore.removeListener('change_rule', this.updateState);
        ParkingStore.removeListener('change_rules', this.updateState);
    }

    handleClick(row) {
        const { rule } = this.state;
        if (rule.time != row.time) {
            ParkingStore.setRule(row);
        }
    }

    renderRow(row, index) {
        let icon = '';
        const { rule } = this.state;

        if (row.time == rule.time) {
            icon = <Icon icon='ion-ios-checkmark' className='icon-big text-success' />;
        }

        return (
            <ListItem key={index} onClick={this.handleClick.bind(this, row)}>
                <div className='center'>
                    <div className="list__item__title">{row.text}</div>
                </div>
                <div className="right">
                    <span className="value">1 CAD = {row.time} minutos</span>
                    &nbsp;
                    {icon}
                </div>
            </ListItem>
        );
    }

    handleBack() {
        PageStore.goHome();
    }

    render() {
        const { rules } = this.state;

        return (
            <Page renderToolbar={this.props.renderToolbar}>
                <List
                    dataSource={rules}
                    renderRow={this.renderRow.bind(this)}
                    renderHeader={() => <ListHeader>tipo de cad</ListHeader>} />
                <br />
                <div className="text-center">
                    Verifique sempre as placas de sinalização.
                    <br />
                    Use no máximo 2 CADs na mesma vaga.
                </div>
                <br />
                <Button
                    className="block text-center uppercase"
                    onClick={this.handleBack.bind(this)}>
                    Confirmar
                </Button>
            </Page>
        );
    }
}
