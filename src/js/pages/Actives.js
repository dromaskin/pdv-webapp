import React from "react";
import { Page, BackButton, List, ListHeader, ListItem, Icon } from 'react-onsenui';

import Header from '../components/Header';
import UserStore from '../stores/UserStore';
import PageStore from '../stores/PageStore';
import MessageStore from '../stores/MessageStore';
import DateTime from '../utils/DateTime';
import GetUserInfo from '../requests/GetUserInfo';
import CancelScheduled from '../requests/CancelScheduled';
import StoreUserInfo from '../requests/StoreUserInfo';

export default class Actives extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
    }

    initialState() {
        return {
            orders: UserStore.getActives()
        }
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        UserStore.on('change_actives', this.updateState);
    }

    componentWillUnmount() {
        UserStore.removeListener('change_actives', this.updateState);
    }

    handleClick(row) {
        UserStore.setActiveParking(null);
        UserStore.setActiveParkingId(row.order_id);
        PageStore.setName('activeParking');
    }

    renderRow(row, index) {
        if (row == 'error') {
            return (
                <ListItem key={index} modifier='longdivider'>
                    <div className='center'>
                        <div className="list__item__title title">
                            Nenhuma vaga ativa
                        </div>
                    </div>
                </ListItem>
            );
        }

        const validFrom = new Date(row.valid_from);
        const validTo = new Date(row.valid_to);

        return (
            <ListItem key={index} modifier='longdivider' onClick={this.handleClick.bind(this, row)}>
                <div className='left'>
                    <div className="list__item__title">{row.amount} CAD{row.amount > 1 ? 's' : ''}</div>
                </div>
                <div className='center'>
                    <div className="list__item__title title">
                        Início {DateTime.stringTime(validFrom)} {row.plate}
                    </div>
                </div>
            </ListItem>
        );
    }

    render() {
        const { orders } = this.state;

        let fields;
        if (orders && orders.length > 0) {
            fields = orders;
        } else {
            fields = ['error'];
        }

        return (
            <Page renderToolbar={this.props.renderToolbar}>
                <Header />
                <List
                    dataSource={fields}
                    renderRow={this.renderRow.bind(this)}
                    renderHeader={() => <ListHeader>vagas ativas</ListHeader>} />
            </Page>
        );
    }
}
