import React from "react";

import { Dialog, Button } from 'react-onsenui';
import Text from '../components/Terms/Text';

export default class Terms extends React.Component {
    handleClose() {
        this.props.onClose();
    }

    render() {
        return (
            <Dialog
                isOpen={this.props.show}
                isCancelable={false}
                onCancel={this.handleClose.bind(this)}
                className="dialog-huge">
                <div className="terms">
                    { this.props.show ? <Text /> : ''}
                </div>
                <br />
                <Button onClick={this.handleClose.bind(this)}>
                    Fechar
                </Button>
            </Dialog>
        );
    }
}
