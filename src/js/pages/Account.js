import React from "react";

import { Page, List, ListHeader, ListItem, Range, Icon } from 'react-onsenui';

import Header from "../components/Header";
import UserStore from '../stores/UserStore';
import PageStore from '../stores/PageStore';
import GetOrders from '../requests/GetOrders';
import DateTime from '../utils/DateTime';
import Constants from '../constants/AppConstants';

export default class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.initialState = this.initialState.bind(this);
        this.state = this.initialState();
        this.filterLabels = Constants.filterLabels;
        this.timer = null;
    }

    initialState() {
        return {
            orders: UserStore.getOrders(),
            userData: UserStore.getData(),
            filter: this.state && typeof this.state.filter == 'number' ?
                this.state.filter :
                Constants.filterLabels.length-1,
            loading: this.state && typeof this.state.loading == 'boolean' ?
                this.state.loading :
                true,
            error: this.state && typeof this.state.error == 'boolean' ?
                this.state.error :
                false
        }
    }

    updateState() {
        this.setState(this.initialState());
    }

    componentWillMount() {
        UserStore.on('change_orders', this.updateState);
    }

    componentWillUnmount() {
        UserStore.removeListener('change_orders', this.updateState);
    }

    componentDidMount() {
        const me = this;
        const { filter } = this.state;
        const period = this.filterLabels[filter].key;
        GetOrders(period, (response) => {
            if (response && response.status) {
                UserStore.setOrders(response.data);
                if (PageStore.getName() == 'account') {
                    me.setState({
                        loading: false
                    });
                }
            } else {
                if (PageStore.getName() == 'account') {
                    me.setState({
                        loading: false,
                        error: true
                    });
                }
            }
        });
    }

    handleNavigate(page) {
        PageStore.setName(page);
    }

    handleClickOrder(order) {
        UserStore.setOrder(order.id);
        PageStore.setName('receipt');
    }

    handleChangeFilter(e) {
        const me = this;
        const filter = parseInt(e.target.value);
        const period = this.filterLabels[filter].key;

        me.setState({
            filter: filter
        });

        if (me.timer) {
            clearTimeout(me.timer);
        }

        me.timer = setTimeout(() => {
            me.setState({
                loading: true
            });
            GetOrders(period, (response) => {
                if (response && response.status) {
                    UserStore.setOrders(response.data);
                    me.setState({
                        loading: false
                    });
                } else {
                    me.setState({
                        loading: false,
                        error: true
                    });
                }
            });
        }, 1000);
    }

    renderRow(row, index) {
        return <ListItem modifier={"chevron"}
                key={index}
                onClick={this.handleNavigate.bind(this, 'profile')}>
                <div className="center">
                    <div className="list__item__title title">Meu Perfil</div>
                </div>
            </ListItem>;
    }

    renderRowReceipts(row, index) {
        switch (row) {
            case 'loading':
                return <ListItem
                        key={index}>
                        <div className="center">
                            <div className="list__item__title title">
                                Carregando Compras
                            </div>
                        </div>
                    </ListItem>;
            case 'error':
                return <ListItem
                        key={index}>
                        <div className="center">
                            <div className="list__item__title title">
                                Erro ao carregar compras
                            </div>
                        </div>
                    </ListItem>;
            case 'empty':
                return <ListItem
                        key={index}>
                        <div className="center">
                            <div className="list__item__title title">
                                Nenhuma compra registrada
                            </div>
                        </div>
                    </ListItem>;
            case 'filter':
                const { filter } = this.state;
                const text = this.filterLabels[filter].text;
                return <ListItem
                        key={index}>
                        <div className="left">
                            <div className="list__item__title">
                                {text}
                            </div>
                        </div>
                        <div className="center">
                            <Range
                                key={"filter"}
                                onChange={this.handleChangeFilter.bind(this)}
                                min={0}
                                max={(this.filterLabels.length-1)}
                                name='filter'
                                value={filter}
                                style={{width: "98%"}} />
                        </div>
                    </ListItem>;
            default:
                if (row.id) {
                    let detail = '';
                    let icon = '';

                    if (row.date) {
                        detail = <span className="value">
                            {DateTime.stringDateFromMysql(row.date)}&nbsp;
                            {DateTime.stringHourFromMysql(row.date)}
                        </span>;
                    }

                    if (row.pre_charger == 1) {
                        icon = <Icon icon='ion-plus' className="text-success icon-baseline" />;
                    } else {
                        icon = <Icon icon='ion-minus' className="text-danger icon-baseline" />;
                    }

                    return (
                        <ListItem
                            key={index}
                            modifier="chevron"
                            onClick={this.handleClickOrder.bind(this, row)}>
                            <div className='center'>
                                <div className="list__item__title">
                                    {icon} {row.amount} CAD{row.amount > 1 ? 's' : ''}
                                    {row.pre_charger == 0 && row.plate ?
                                        ' ' + row.plate : ''}
                                </div>
                            </div>
                            <div className="right">
                                {detail}
                            </div>
                        </ListItem>
                    );
                }

                return '';
        }
    }

    render() {
        const { loading, error, orders, userData } = this.state;
        let fields = [];
        let profileField = '';

        if (!userData.guest) {
            profileField = <List
                dataSource={['profile']}
                renderRow={this.renderRow.bind(this)}
                renderHeader={() => <ListHeader>Meus dados</ListHeader>} />;
        }

        if (loading) {
            fields.push('loading');
        } else if (error) {
            fields.push('filter');
            fields.push('error');
        } else if (!(orders && orders.length > 0)) {
            fields.push('filter');
            fields.push('empty');
        } else {
            fields = orders;
            if (fields[0] != 'filter') {
                fields.unshift('filter');
            }
        }

        return (
            <Page renderToolbar={this.props.renderToolbar}>
                <Header disabled={true} />
                {profileField}
                <List
                    dataSource={fields}
                    renderRow={this.renderRowReceipts.bind(this)}
                    renderHeader={() => <ListHeader>Minhas Compras</ListHeader>} />
            </Page>
        );
    }
}
